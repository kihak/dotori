//
//  UIView+copying.h
//  dotori
//
//  Created by kkh on 2014. 6. 23..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (copying)<NSCopying>
- (id)copyWithZone:(NSZone *)zone ;
- (id)clone;
@end
