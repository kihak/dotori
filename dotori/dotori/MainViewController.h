//
//  MainViewController.h
//  dotori
//
//  Created by kihak on 2014. 5. 13..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KHViewController.h"
#import "NJKWebViewProgress.h"
#import "UICollectionView+Draggable.h"


@interface MainViewController : KHViewController<UICollectionViewDataSource_Draggable, UICollectionViewDelegate>
{
    UICollectionView* _collectionView ;
    
}

@property (nonatomic, strong) UIBarButtonItem *editBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *addBarButtonItem;


-(void)addWebViewController:(NSURLRequest *)request frame:(CGRect)frame;
-(void)reflash;
-(void)closedMenu;
-(void)Addaddress;


@end
