//
//  MainView.h
//  kafari
//
//  Created by kihak on 2014. 4. 8..
//  Copyright (c) 2014년 smartkt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KHWebView.h"



@interface MainView : UIView

@property (nonatomic)KHWebView* CurWebView;
@property (nonatomic)NSMutableArray* arryForwordViews;

@property (nonatomic)UIFont * menuItemsFont;


@property (nonatomic)int textSize;
- (void) didchangeOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;
- (void) willchangeOrientation:(UIInterfaceOrientation)toInterfaceOrientation;


@end
