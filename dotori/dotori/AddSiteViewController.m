//
//  AddSiteViewController.m
//  dotori
//
//  Created by kkh on 2014. 5. 29..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#define SIZE_TEXTFIELD_HEIGHT 40

#import "AddSiteViewController.h"
#import "shareDataManager.h"
#import "INCCustomContainer.h"

@interface AddSiteViewController ()
{
    UITextField* _addresstextfield;
    UIView* _addressview;
    UIButton* _httpBtn;
    UIButton* _addBtn;
    NSMutableArray* _arryhttplist;
    int       _seletedindex;
    
}

@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (nonatomic, strong) UIGravityBehavior *gravityBeahvior;
@property (nonatomic, strong) UICollisionBehavior *collisionBehavior;
@property (nonatomic, strong) UIDynamicItemBehavior *itemBehavior;

@end


@implementation AddSiteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor whiteColor]];

        
        

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    _addressview = [[UIView alloc]initWithFrame:CGRectZero];
    
    // Do any additional setup after loading the view.
    _addresstextfield = [[UITextField alloc]init];
    [_addresstextfield setKeyboardType:UIKeyboardTypeURL];
    [_addresstextfield setBorderStyle:UITextBorderStyleLine];
    [_addresstextfield setFrame:_addressview.frame];
    _addresstextfield.delegate =  self;
    [_addressview addSubview:_addresstextfield];
    
    _seletedindex = 0;
    _arryhttplist = [[NSMutableArray alloc] initWithObjects:@"http" , @"https",@"ftp" , @"self",nil];
    
  
    [self.view addSubview:_addressview];
    
    
    _httpBtn = [[UIButton alloc]initWithFrame:CGRectZero];
    _httpBtn.backgroundColor = [UIColor blackColor];
    [_httpBtn setTitle : _arryhttplist[_seletedindex] forState : UIControlStateNormal];
    _httpBtn.titleLabel.backgroundColor = [UIColor clearColor];
    _httpBtn.titleLabel.textColor = [UIColor whiteColor];
    [_httpBtn addTarget:self action: @selector(showActionSheet:) forControlEvents:UIControlEventTouchUpInside];
    
    [_addressview addSubview:_httpBtn];

    _addBtn = [[UIButton alloc]initWithFrame:CGRectZero];
    _addBtn.backgroundColor = [UIColor blackColor];
    [_addBtn setTitle : @"+" forState : UIControlStateNormal];
    _addBtn.titleLabel.backgroundColor = [UIColor clearColor];
    _addBtn.titleLabel.textColor = [UIColor whiteColor];
    [_addBtn addTarget:self action: @selector(onadd:) forControlEvents:UIControlEventTouchUpInside];
    
    [_addressview addSubview:_addBtn];

    
    [_addresstextfield setFrame:CGRectMake(0,0, 50 ,SIZE_TEXTFIELD_HEIGHT)];
    [_addressview addSubview:_httpBtn];
    
    
        // Set up
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    self.gravityBeahvior = [[UIGravityBehavior alloc] initWithItems:nil];
    
    self.collisionBehavior = [[UICollisionBehavior alloc] initWithItems:nil];
    self.collisionBehavior.translatesReferenceBoundsIntoBoundary = YES;
    
    self.itemBehavior = [[UIDynamicItemBehavior alloc] initWithItems:nil];
    self.itemBehavior.elasticity = 0.6;
    self.itemBehavior.friction = 0.5;
    self.itemBehavior.resistance = 0.5;
    
    
    [self.animator addBehavior:self.gravityBeahvior];
    [self.animator addBehavior:self.collisionBehavior];
    [self.animator addBehavior:self.itemBehavior];
    
 
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

//    NSArray *segmentTextContent = @[
//                                    NSLocalizedString(@"직접입력", @""),
//                                    NSLocalizedString(@"사이트리스트", @"")
//                                    ];
   
//    // Segmented control as the custom title view
//	UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:segmentTextContent];
//	segmentedControl.selectedSegmentIndex = 0;
//	segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//	segmentedControl.frame = CGRectMake(0, 0, 200.0f, 30.0f);
//	[segmentedControl addTarget:self action:@selector(action:) forControlEvents:UIControlEventValueChanged];
//	
//	self.navigationItem.titleView = segmentedControl;
    
    
}

- (UIBarButtonItem *)closeBarButtonItem {
    if (!_closeBarButtonItem) {
        _closeBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                            target:self
                                                                            action:@selector(closeClicked:)];
		_closeBarButtonItem.width = 18.0f;
    }
    return _closeBarButtonItem;
}

-(void)closeClicked:(id)sender
{
        // [self removeFromParentViewController];
    [self.customContainer  completeTransition];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *items = [NSArray arrayWithObjects:
                      fixedSpace,
                      self.closeBarButtonItem,
                      flexibleSpace,
                      flexibleSpace,
                      flexibleSpace,
                      flexibleSpace,
                      flexibleSpace,
                      flexibleSpace,
                      fixedSpace,
                      nil];
    self.customContainer.bottomBar.items = items;
    
    
}

- (void)keyboardWillShow:(NSNotification *)note
{
     for( UIWindow *keyboardWindow in [[UIApplication sharedApplication] windows] )
    {
            //      for( UIView *keyboard in [keyboardWindow subviews] )
        {
                //            NSString *desc = [keyboard description];
                //           if([desc hasPrefix:@"<UIPeripheralHostView"])
            {
//                [self.gravityBeahvior addItem:_addressview];
//                [self.collisionBehavior addItem:_addressview];
//                [self.itemBehavior addItem:_addressview];
//                
                    //                [self.collisionBehavior addItem:keyboardWindow];

            }
        }
    }

//    [self.gravityBeahvior addItem:_addressview];
//    [self.collisionBehavior addItem:_addressview];
//    [self.itemBehavior addItem:_addressview];
 
}

- (void)keyboardWillHide:(NSNotification *)note
{

}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
     

    return 1;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{


}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string 
{
    
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
 
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( [_arryhttplist count] > buttonIndex) {
        _seletedindex = buttonIndex;
        [_httpBtn setTitle : _arryhttplist[_seletedindex] forState : UIControlStateNormal];
    }
    NSLog(@"You have pressed the %@ button %d", [actionSheet buttonTitleAtIndex:buttonIndex],buttonIndex);
}

- (void)onadd:(id)sender
{
    
    
    UIAlertView *alertView1 = [[UIAlertView alloc] initWithTitle:@"Enter Form Title" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alertView1.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *myTextField = [alertView1 textFieldAtIndex:0];
    [alertView1 setTag:555];
    myTextField.keyboardType=UIKeyboardTypeDefault;
    
    [alertView1 show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	
    UITextField * alertTextField = [alertView textFieldAtIndex:0];
    NSLog(@"alerttextfiled - %@",alertTextField.text);
    
    
	// 사용자가 Yes를 선택한 경우
	if (buttonIndex == 1) {
		
        if (_seletedindex == 3)
        {
            NSString* url = [NSString stringWithFormat:@"%@", _addresstextfield.text];
            [[shareDataManager Instance].arryuserdata addObject:
             @{JSON_KEY_TYPE:@"1", JSON_KEY_IMAGE_NAME:@"",JSON_KEY_URL:url,JSON_KEY_NAME:alertTextField.text,JSON_KEY_IMAGE:@"'"}
             ];
            
        }else
        {
            NSString* url = [NSString stringWithFormat:@"%@://%@", _arryhttplist[_seletedindex] ,_addresstextfield.text];
            [[shareDataManager Instance].arryuserdata addObject:
             @{JSON_KEY_TYPE:@"1", JSON_KEY_IMAGE_NAME:@"",JSON_KEY_URL:url,JSON_KEY_NAME:alertTextField.text,JSON_KEY_IMAGE:@"'"}
             ];
            
        }

        
        [self.navigationController popViewControllerAnimated:YES];
	}else
    {
     
        
    }
}

- (void)showActionSheet:(id)sender
{
   
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: nil
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: nil];
    
    for (NSString *title in _arryhttplist) {
        [actionSheet addButtonWithTitle: title];
    }
    
    [actionSheet addButtonWithTitle: @"Cancel"];
    [actionSheet setCancelButtonIndex: [_arryhttplist count]];
    [actionSheet showInView:self.view];
    
    
}

#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return _arryhttplist.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return _arryhttplist[row];
}


#pragma mark PickerView Delegate
// tell the picker the width of each row for a given component
//- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
//    int sectionWidth = 300;
//    
//    return sectionWidth;
//}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
//    float rate = [_exchangeRates[row] floatValue];
//    float dollars = [_dollarText.text floatValue];
//    float result = dollars * rate;
//    
//    NSString *resultString = [[NSString alloc] initWithFormat:
//                              @"%.2f USD = %.2f %@", dollars, result,
//                              _countryNames[row]];
//    _resultLabel.text = resultString;
}
-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 44)];
    label.backgroundColor = [UIColor lightGrayColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    label.text = [NSString stringWithFormat:@"%@",_arryhttplist[row]];
    return label;
}


- (void)action:(id)sender
{
	NSLog(@"-[%@ %@], Selected segment is: %zi", [self class], NSStringFromSelector(_cmd), [sender selectedSegmentIndex]);
    
    switch ([sender selectedSegmentIndex]) {
        case 0:
        {
          }
            break;
        case 1:
        {
        }
            break;
        case 2:
            
            break;
            
        default:
            break;
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self changewebview];
}

-(void)changewebview
{
    [_addressview setFrame:CGRectMake(self.view.frame.size.width/20, 20,self.view.frame.size.width/20*18, SIZE_TEXTFIELD_HEIGHT)];
 
    float x = (_addressview.frame.size.width/8);
    
    [_httpBtn setFrame:CGRectMake(0, 0,x, SIZE_TEXTFIELD_HEIGHT)];
    [_addresstextfield setFrame:CGRectMake(x, 0, x*6, SIZE_TEXTFIELD_HEIGHT)];
    [_addBtn setFrame:CGRectMake(x+x*6, 0, x, SIZE_TEXTFIELD_HEIGHT)];
        
}

@end
