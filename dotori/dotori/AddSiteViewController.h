//
//  AddSiteViewController.h
//  dotori
//
//  Created by kkh on 2014. 5. 29..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KHViewController.h"


@interface AddSiteViewController : KHViewController<UIAlertViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,UIPickerViewDelegate, UIPickerViewDataSource>
-(void)changewebview;
@property (nonatomic, strong) UIBarButtonItem *closeBarButtonItem;

@end
