//
//  WebSiteTableViewController.m
//  dotori
//
//  Created by kkh on 2014. 6. 3..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import "WebSiteTableViewController.h"
#import "shareDataManager.h"
#import "INCCustomContainer.h"
#import "MainViewController.h"

@interface WebSiteTableViewController ()
{
    NSMutableArray* _data;
    
}
@end

@implementation WebSiteTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _data = [[shareDataManager Instance].arryuserdata mutableCopy];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.tableView reloadData];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *items = [NSArray arrayWithObjects:
                      fixedSpace,
                      self.closeBarButtonItem,
                      flexibleSpace,
                      self.addBarButtonItem,
                      flexibleSpace,
                      flexibleSpace,
                      flexibleSpace,
                      flexibleSpace,
                      fixedSpace,
                      nil];
    self.customContainer.bottomBar.items = items;
    

}
- (UIBarButtonItem *)closeBarButtonItem {
    if (!_closeBarButtonItem) {
        _closeBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                           target:self
                                                                           action:@selector(closeClicked:)];
		_closeBarButtonItem.width = 18.0f;
    }
    return _closeBarButtonItem;
}

- (UIBarButtonItem *)addBarButtonItem {
    if (!_addBarButtonItem) {
        _addBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                            target:self
                                                                            action:@selector(addClicked:)];
        _addBarButtonItem.width = 18.0f;
    }
    return _addBarButtonItem;
}


-(void)addClicked:(id)sender
{
    [(MainViewController*)[shareDataManager Instance].mainViewController  Addaddress];
}

-(void)closeClicked:(id)sender
{
   // [self removeFromParentViewController];
    [self.customContainer  completeTransition];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return  [[shareDataManager Instance].arryuserdata count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_site"];
    UIImageView *imageView;
    UILabel* titleLabel;
    UILabel* subTitleLabel;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleValue1 reuseIdentifier: @"cell_site"];
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 30, 30)];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        //self.imageView.hidden = YES;
        // add a white frame around the image
        imageView.layer.borderWidth = 1.0;
        imageView.layer.borderColor = [UIColor whiteColor].CGColor;
        imageView.layer.edgeAntialiasingMask =
        kCALayerLeftEdge | kCALayerRightEdge | kCALayerBottomEdge | kCALayerTopEdge;

        imageView.tag = 100;
        [[cell contentView] addSubview:imageView];

        float posY = 5;
        
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, posY, self.view.bounds.size.width, 20)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [UIColor blueColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:12];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.tag = 101;
        [[cell contentView] addSubview:titleLabel ];

        subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,posY+22,self.view.bounds.size.width, 20)];
        subTitleLabel.backgroundColor = [UIColor clearColor];
        subTitleLabel.textColor = [UIColor blueColor];
        subTitleLabel.font = [UIFont systemFontOfSize:10];
        subTitleLabel.textAlignment = NSTextAlignmentLeft;
        subTitleLabel.tag = 102;
        //  [self.subTitleLabel sizeToFit];
        [[cell contentView] addSubview:subTitleLabel ];

    }else
    {
        imageView = (UIImageView*)[cell.contentView viewWithTag:100];
        titleLabel = (UILabel*)[cell.contentView viewWithTag:101] ;
        subTitleLabel = (UILabel*)[cell.contentView viewWithTag:102] ;
        
    }
    
 
    NSDictionary* dic =   [[shareDataManager Instance].arryuserdata  objectAtIndex:indexPath.row];
    // create our image view so that is matches the height and width of this cell
    [imageView setImage:[shareDataManager loadImagefile:[dic objectForKey:JSON_KEY_IMAGE_NAME]]];
    // Define how the edges of the layer are rasterized for each of the four edges
    // (left, right, bottom, top) if the corresponding bit is set the edge will be antialiased
    //
    
    titleLabel.text = [dic objectForKey:JSON_KEY_NAME];
    subTitleLabel.text = [dic objectForKey:JSON_KEY_URL];
    //  [self.titleLabel sizeToFit];
        
    // Configure the cell...
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
      [[shareDataManager Instance].arryuserdata removeObjectAtIndex:indexPath.row];
      [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
      [shareDataManager saveJsonfile:FILE_JSON_DEFAULT];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
