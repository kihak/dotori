//
//  WebSiteTableViewController.h
//  dotori
//
//  Created by kkh on 2014. 6. 3..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebSiteTableViewController : UITableViewController

@property (nonatomic, strong) UIBarButtonItem *closeBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *addBarButtonItem;

@end
