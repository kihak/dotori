//
//  TTSView.m
//  kafari
//
//  Created by kihak on 2014. 4. 9..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import "TTSView.h"




@implementation TTSView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor orangeColor];
        self.alpha = 1.0;
 
        
    
    }
    return self;
}

-(void)initdata
{
    CGFloat scaleFactor = 1.0;
    
    
    self.TextView  = [[UITextView alloc]initWithFrame:CGRectMake(0, 40, self.frame.size.width, self.frame.size.height)];
    self.TextView.textAlignment = NSTextAlignmentLeft;
    self.TextView.attributedText = self.ttstext;
//   self.TextView .font					= [UIFont KHFontStyleFontForTextStyle:UIFontTextStyleBody scale:1.0];
    self.TextView .textColor				= [UIColor blackColor];
    self.TextView .backgroundColor		= [UIColor whiteColor];
    self.TextView .keyboardType				= UIKeyboardTypeDefault;
    self.TextView .autocorrectionType		= UITextAutocorrectionTypeNo;
    self.TextView .autocapitalizationType	= UITextAutocapitalizationTypeNone;
  
    
  //  [self.TextView setFont:[UIFont KHFontStyleFontForTextStyle:UIFontTextStyleBody scale:scaleFactor]];
    [self addSubview:self.TextView];
    
    UIButton* startBtn = [[UIButton alloc]initWithFrame:CGRectZero];
    startBtn.backgroundColor = [UIColor blackColor];
    [startBtn setTitle : @ "close" forState : UIControlStateNormal];
    startBtn.titleLabel.backgroundColor = [UIColor clearColor];
    startBtn.titleLabel.textColor = [UIColor whiteColor];
    [startBtn setFrame:CGRectMake(self.frame.size.width/3 - (335/3)/2, 20, 50, 20)];
    [startBtn addTarget:self action: @selector(onClose:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:startBtn];
  
    
    UIButton* speakBtn = [[UIButton alloc]initWithFrame:CGRectZero];
    speakBtn.backgroundColor = [UIColor blackColor];
    [speakBtn setTitle : @ "tts" forState : UIControlStateNormal];
    speakBtn.titleLabel.backgroundColor = [UIColor clearColor];
    speakBtn.titleLabel.textColor = [UIColor whiteColor];
    [speakBtn setFrame:CGRectMake(self.frame.size.width/2 - (335/3)/2, 20, 50, 20)];
    [speakBtn addTarget:self action: @selector(onSpeak:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:speakBtn];

    [self synthesizer];
}

- (AVSpeechSynthesizer *)synthesizer
{
    if (!_synthesizer)
    {
        _synthesizer = [[AVSpeechSynthesizer alloc] init];
        _synthesizer.delegate = self;
    }
    return _synthesizer;
}


- (NSArray *)languageCodes
{
    if (!_languageCodes)
    {
        _languageCodes = [self.languageDictionary keysSortedByValueUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    }
    return _languageCodes;
}



- (NSDictionary *)languageDictionary
{
    if (!_languageDictionary)
    {
        NSArray *voices = [AVSpeechSynthesisVoice speechVoices];
        NSArray *languages = [voices valueForKey:@"language"];
        
        NSLocale *currentLocale = [NSLocale autoupdatingCurrentLocale];
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        for (NSString *code in languages)
        {
            dictionary[code] = [currentLocale displayNameForKey:NSLocaleIdentifier value:code];
        }
        _languageDictionary = dictionary;
    }
    return _languageDictionary;
}

- (void)stopSpeech
{
    if([self.synthesizer isSpeaking]) {
        [self.synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
//        AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:@""];
//        [self.synthesizer speakUtterance:utterance];
 //       [self.synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    }
}


- (void)onSpeak:(id)sender
{
    if ([self.synthesizer isPaused]) {
        
        [self.synthesizer  continueSpeaking];
        return;
    }

    if ([self.synthesizer isSpeaking]) {
        [self stopSpeech];
        return;
    }
    
    NSString *currentLanguageCode = [AVSpeechSynthesisVoice currentLanguageCode];

    
    [self languageDictionary];
    [self languageCodes];
    NSString *languageCode = self.languageCodes[0];
    NSString *languageName = self.languageDictionary[currentLanguageCode];
    
    AVSpeechUtterance *utterance =
    [AVSpeechUtterance speechUtteranceWithString:self.TextView.text];
    utterance.rate = AVSpeechUtteranceMaximumSpeechRate / 4.0f;
    utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:currentLanguageCode]; // defaults to your system language
    [self.synthesizer speakUtterance:utterance];
    
}

-(void)onClose:(id)sender
{
    if([self.synthesizer isSpeaking]) {
        [self.synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    }
    [
     self removeFromSuperview];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
