//
//  shareDataManager.h
//  kafari
//
//  Created by kihak on 2014. 4. 8..
//  Copyright (c) 2014년 smartkt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainViewController.h"
#import "defines.h"


@interface shareDataManager : NSObject

@property(nonatomic)MainViewController      *mainViewController;
@property(nonatomic)NSMutableArray          *arryuserdata;


+ (shareDataManager *)Instance;

- (void)initInstance;
- (void)initData;
+ (UIImage*)MakeImageFromView:(UIView *)view;
- (void)loadJsonfile:(NSString*)fileName;
- (bool)saveJsonfile:(NSString*)fileName data:(NSDictionary*)datadic;
- (void)defaultdataset:(NSString*)fileName;
- (void)urlImagefileSave;
- (void)saveImageFile:(NSString *)fileUrl filename:(NSString*)fileName;
+ (UIImage*)loadImagefile:(NSString*)fileName;
+ (void)saveImageFileUrl:(NSString *)fileUrl filename:(NSString*)fileName;
+ (void)saveJsonfile:(NSString*)fileName;

@end
