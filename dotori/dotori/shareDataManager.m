//
//  shareDataManager.m
//  kafari
//
//  Created by kihak on 2014. 4. 8..
//  Copyright (c) 2014년 smartkt. All rights reserved.
//

#import "shareDataManager.h"

@implementation shareDataManager
@synthesize arryuserdata;


static shareDataManager *Instance = nil;

+(shareDataManager *)Instance
{
	if( Instance == nil )
	{
		Instance = [self new];
		
		[Instance initInstance];
	}
	
	return Instance;
}

-(void)initInstance
{
	[self initData];
}

- (void)initData
{
    [Instance loadJsonfile:FILE_JSON_DEFAULT];
}

- (void)dealloc
{
    
}


+ (UIImage *)MakeImageFromView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0f);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:NO];
    UIImage * snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshotImage;
}

+ (UIImage*)loadImagefile:(NSString*)fileName
{
    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * path = [documents stringByAppendingString:[NSString stringWithFormat:@"/%@/%@", PATH_FILE_IMAGE , fileName]];
      
    return  [UIImage imageWithContentsOfFile:path];
}

- (void)loadJsonfile:(NSString*)fileName
{
    
    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@/%@", documents, PATH_FILE_DATA ,fileName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError * error = nil;
    if ([fileManager fileExistsAtPath:filePath] == FALSE)
    {
        [Instance defaultdataset:fileName];
    }
    
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    
    NSMutableDictionary* result = [NSJSONSerialization JSONObjectWithData:data
                                                                  options:NSJSONReadingMutableContainers
                                                                    error:&error];
    if (error != nil)
    {
        
    }
   
    if (result == nil) {
        
    }
    
    Instance.arryuserdata = [result objectForKey:JSON_KEY_DATA];
    [Instance urlImagefileSave];
    
    
  // [Instance.arryuserdata addObject: @{JSON_KEY_TYPE:@"2",JSON_KEY_IMAGE_NAME:@"",JSON_KEY_URL:@"",JSON_KEY_NAME:@"",JSON_KEY_IMAGE:@""}];

}


- (void)urlImagefileSave
{
    for (NSDictionary* dic in  Instance.arryuserdata)
    {
        if ([[dic objectForKey:JSON_KEY_IMAGE] isEqualToString:@""] == FALSE) {
            [Instance saveImageFile:[dic objectForKey:JSON_KEY_IMAGE] filename:[dic objectForKey:JSON_KEY_IMAGE_NAME]];
        }
    }
}


- (void)defaultdataset:(NSString*)fileName
{
    
    NSArray* arry = @[
       @{JSON_KEY_TYPE:@"1",JSON_KEY_IMAGE_NAME:@"mobile_180026457661.png",JSON_KEY_URL:@"http://naver.com",JSON_KEY_NAME:@"네이버",JSON_KEY_IMAGE:@"http://static.naver.net/www/mobile/edit/2014/0331/mobile_180026457661.png"},
       @{JSON_KEY_TYPE:@"1",JSON_KEY_IMAGE_NAME:@"favicon.ico",JSON_KEY_URL:@"http://daum.net",JSON_KEY_NAME:@"다음",JSON_KEY_IMAGE:@"http://m.daum.net/favicon.ico"},
       @{JSON_KEY_TYPE:@"1",JSON_KEY_IMAGE_NAME:@"",JSON_KEY_URL:@"http://google.com",JSON_KEY_NAME:@"구글",JSON_KEY_IMAGE:@""},
       @{JSON_KEY_TYPE:@"1",JSON_KEY_IMAGE_NAME:@"nate_114.png",JSON_KEY_URL:@"http://nate.com",JSON_KEY_NAME:@"네이트",JSON_KEY_IMAGE:@"http://m1.nateimg.co.kr/main/nate_114.png"}
    ];
 
    if([Instance saveJsonfile:fileName data:@{JSON_KEY_DATA:arry}])
    {
        NSLog(@"%s : defaultdataset failed" , __PRETTY_FUNCTION__  );
    }
    
    
}

- (void) saveImageFile:(NSString *)fileUrl filename:(NSString*)fileName
{
    if ([fileName isEqualToString:@""] || [fileUrl isEqualToString:@""]) {
        return;
    }
    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * path = [documents stringByAppendingString:[NSString stringWithFormat:@"/%@/", PATH_FILE_IMAGE]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError * error = nil;
    if ([fileManager fileExistsAtPath:path] == FALSE)
        [fileManager createDirectoryAtPath:path withIntermediateDirectories:TRUE attributes:nil error:&error];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fileUrl]];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage* imageToSave = [[UIImage alloc]initWithData:data];
                                   NSData * binaryImageData = UIImagePNGRepresentation(imageToSave);
                                   [binaryImageData writeToFile:[NSString stringWithFormat:@"%@/%@", path, fileName] atomically:NO];
                                  
                               } else{
                                   
                               }
                           }];

    
    
 

}

+ (void) saveImageFileUrl:(NSString *)fileUrl filename:(NSString*)fileName
{
    if ([fileName isEqualToString:@""] || [fileUrl isEqualToString:@""]) {
        return;
    }
    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * path = [documents stringByAppendingString:[NSString stringWithFormat:@"/%@/", PATH_FILE_IMAGE]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError * error = nil;
    if ([fileManager fileExistsAtPath:path] == FALSE)
        [fileManager createDirectoryAtPath:path withIntermediateDirectories:TRUE attributes:nil error:&error];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fileUrl]];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage* imageToSave = [[UIImage alloc]initWithData:data];
                                   NSData * binaryImageData = UIImagePNGRepresentation(imageToSave);
                                   [binaryImageData writeToFile:[NSString stringWithFormat:@"%@/%@", path, fileName] atomically:NO];
                                   
                               } else{
                                   
                               }
                           }];
    
    
    
    
    
}

+ (void)saveJsonfile:(NSString*)fileName
{
    if([Instance saveJsonfile:fileName data:@{JSON_KEY_DATA:Instance.arryuserdata}])
    {
        NSLog(@"%s : defaultdataset failed" , __PRETTY_FUNCTION__  );
    }
}


- (bool)saveJsonfile:(NSString*)fileName data:(NSDictionary*)datadic
{
    
    if (datadic == nil)
        return false;
    
    
    NSError * err = nil;
    NSData * innerMessageData = [NSJSONSerialization  dataWithJSONObject:datadic options:0 error:&err];
    
    if (err != nil || innerMessageData == nil)
        return false;
    
    NSError *parseError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:innerMessageData  options:NSJSONReadingAllowFragments error:&parseError];
    
    if (err != nil || jsonObject== nil)
        return false;
    
    // initialize and open the stream
    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *file = [NSString stringWithFormat:@"%@/%@/%@", documents,PATH_FILE_DATA,fileName];
    NSString * path = [documents stringByAppendingString:[NSString stringWithFormat:@"/%@",PATH_FILE_DATA]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError * error = nil;
    if ([fileManager fileExistsAtPath:path] == FALSE)
        [fileManager createDirectoryAtPath:path withIntermediateDirectories:TRUE attributes:nil error:&error];
    
    if (error != nil)
        return false;
    
    NSOutputStream *stream = [[NSOutputStream alloc] initToFileAtPath:file append:NO];
    [stream open];
    // write JSON representation of our tweet array to file
    NSError *writeError = nil;
    NSInteger bytesWritten = [NSJSONSerialization writeJSONObject:jsonObject toStream:stream options:NSJSONWritingPrettyPrinted error:&writeError];
    [stream close];
    
    if (writeError != nil)
        return false;
    if (bytesWritten <= 0) {
        NSLog(@"Error writing JSON Data");
    }
    
    return true;
}


@end
