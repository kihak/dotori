//
//  KHUIWebViewController.h
//  dotori
//
//  Created by kkh on 2014. 5. 16..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NJKWebViewProgress.h"
#import "KHViewController.h"


@interface KHUIWebViewController : KHViewController<UIWebViewDelegate,NJKWebViewProgressDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate>

-(void)loadurl:(NSString*)url index:(int)nindex;
-(void)loadrequest:(NSURLRequest *)request;
-(void)changewebview;

@property (nonatomic, strong) UIBarButtonItem *backBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *forwardBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *refreshBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *listBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *stopBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *actionBarButtonItem;
@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, strong) id    data;

@end
