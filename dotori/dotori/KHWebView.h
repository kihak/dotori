//
//  KHWebView.h
//  kafari
//
//  Created by kihak on 2014. 4. 15..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface KHWebView : UIView<UIWebViewDelegate,NSCopying>
{
    BOOL _interactive;
    NSURL* _currentURL;
}
@property (nonatomic)UIWebView* mainwebview;
@property (nonatomic, strong) UIImage* imageCapture;

- (id)copyWithZone:(NSZone *)zone ;
- (void)loadRequest:(NSURLRequest *)request;
- (void)loadHTMLString:(NSString *)string baseURL:(NSURL *)baseURL;
- (void)chageFont:(int)textSize;
- (BOOL)canGoBack;
- (BOOL)canGoForward;

- (void)goBack;
- (void)goForward;


@end
