//
//  MainView.m
//  kafari
//
//  Created by kihak on 2014. 4. 8..
//  Copyright (c) 2014년 smartkt. All rights reserved.
//

#import "MainView.h"
#import "TTSView.h"




@implementation MainView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
 
//        self.titleview = [[KHView alloc]init];
//        self.titleview.backgroundColor = [UIColor clearColor];
//        [self addSubview:self.titleview];
//        self.titleview.hidden = YES;
//        
//          
//        KHCircleButton* plusebutton  = [[KHCircleButton alloc]initWithFrame:CGRectMake(10, 0, 50, 50)];
//        [plusebutton drawCircleButton:[UIColor redColor]];
//        [plusebutton setTitle: @"F+" forState: UIControlStateNormal];
//        plusebutton.titleLabel.numberOfLines = 0;
//        [plusebutton addTarget: self action: @selector(onPluseButton:) forControlEvents: UIControlEventTouchUpInside];
//        [self.titleview addSubview:plusebutton];
//
//    
//        KHCircleButton* minusbutton  = [[KHCircleButton alloc]initWithFrame:CGRectMake(70, 0, 50, 50)];
//        [minusbutton drawCircleButton:[UIColor redColor]];
//        [minusbutton setTitle: @"F-" forState: UIControlStateNormal];
//        minusbutton.titleLabel.numberOfLines = 0;
//        [minusbutton addTarget: self action: @selector(onMinusButton:) forControlEvents: UIControlEventTouchUpInside];
//        
//        [self.titleview addSubview:minusbutton];
//
//        KHCircleButton* ttsbutton  = [[KHCircleButton alloc]initWithFrame:CGRectMake(130, 0, 50, 50)];
//        [ttsbutton drawCircleButton:[UIColor blueColor]];
//        [ttsbutton setTitle: @"S" forState: UIControlStateNormal];
//        ttsbutton.titleLabel.numberOfLines = 0;
//        [ttsbutton addTarget: self action: @selector(onTTSButton:) forControlEvents: UIControlEventTouchUpInside];
//        
//        [self.titleview addSubview:ttsbutton];
//
//        
//        KHCircleButton* addrbutton  = [[KHCircleButton alloc]initWithFrame:CGRectMake(190, 0, 50, 50)];
//        [addrbutton drawCircleButton:[UIColor brownColor]];
//        [addrbutton setTitle: @"A" forState: UIControlStateNormal];
//        addrbutton.titleLabel.numberOfLines = 0;
//        [addrbutton addTarget: self action: @selector(onInputAddress:) forControlEvents: UIControlEventTouchUpInside];
//        
//        [self.titleview addSubview:addrbutton];

//        self.mainbutton = [[KHCircleButton alloc]initWithFrame:CGRectMake(self.frame.size.width-60, self.frame.size.height-80, 50, 50)];
//        [self.mainbutton drawCircleButton:[UIColor orangeColor]];
//        [self.mainbutton setTitle: @"M" forState: UIControlStateNormal];
//     //   self.mainbutton.backgroundColor = [UIColor blackColor];
//        self.mainbutton.titleLabel.numberOfLines = 0;
//        [self.mainbutton addTarget: self action: @selector(onMainbutton:) forControlEvents: UIControlEventTouchUpInside];
//        [self  addSubview:self.mainbutton ];
 
   
        self.textSize = 100;
        
        UIScreenEdgePanGestureRecognizer* panLeft=  [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(EdgeLeftPanDetected:)];
        panLeft.edges = UIRectEdgeLeft;
        [self addGestureRecognizer:panLeft];


        UIScreenEdgePanGestureRecognizer* panRight=  [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(EdgeRightPanDetected:)];
        panRight.edges = UIRectEdgeRight;
        [self addGestureRecognizer:panRight];

        
        
        self.arryForwordViews = [[NSMutableArray alloc]initWithCapacity:100];
    }
    
 
    
    return self;
}


-(void)onMainbutton:(id)sender
{
//    if (self.titleview.hidden == NO) {
//        self.titleview.hidden = YES;
//        [UIView animateWithDuration:0.2 animations:^{
//            self.mainbutton.frame = CGRectMake(self.frame.size.width-60, self.frame.size.height-80, 50, 50);
//        } completion:^(BOOL finished) {
//   
//        }];
//        
//        
//    }else{
//        
//    
//    [UIView animateWithDuration:0.2 animations:^{
//        self.mainbutton.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
//                 } completion:^(BOOL finished) {
//                     self.titleview.frame = CGRectMake((self.frame.size.width/2-250/2), self.mainbutton.frame.origin.y-60, 250, 50);
//
//                     self.titleview.hidden  = NO;
//                     
//                 }];
//    
//    
//    }
}





-(void)checktextsize
{
    if (_textSize <= 50)
        self.textSize = 50;
    
    if (_textSize >= 200)
        self.textSize = 200;

}

- (void)onInputClose:(NSString*)addrtext
{
    NSMutableString *url = [[NSMutableString alloc] initWithString:addrtext];
    [self initdata:url];


}


-(void)onTTSButton:(id)sender
{
//    TTSView* ttsview  =  [[TTSView alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
//   
//    ttsview.ttstext =[[NSAttributedString alloc]initWithString:[self.CurWebView.mainwebview stringByEvaluatingJavaScriptFromString:@"document.body.innerText"]];
//    
//    //NSString* ff = [self.webView stringByEvaluatingJavaScriptFromString:@"document.body.textContent"];
//    [ttsview initdata];
//    
//    
//    [[shareDataManager instance].rootController.view addSubview:ttsview];
    
}




-(void)onPluseButton:(id)sender
{
    self.textSize +=10;
    [self.CurWebView chageFont:self.textSize];
    
}

-(void)onMinusButton:(id)sender
{
    self.textSize -=10;
    [self.CurWebView chageFont:self.textSize];
    
}


- (void)initframe
{
    self.CurWebView.frame = self.frame;
//    self.titleview.frame = CGRectMake((self.frame.size.width/2-self.titleview.frame.size.width/2), self.mainbutton.frame.origin.y, self.frame.size.width, 30);
    
    
}

- (void)initdata
{
    self.CurWebView = [[KHWebView alloc]init];
    [self initframe];

    self.CurWebView = [self initdata:@"http://naver.com"];
  
}


- (KHWebView*)initdata:(NSString*)url
{
    NSMutableURLRequest* request =  [NSURLRequest requestWithURL: [NSURL URLWithString: url]
                                                     cachePolicy: NSURLRequestUseProtocolCachePolicy
                                                 timeoutInterval: 60.0];

  //      [[NSURLCache sharedURLCache] removeCachedResponseForRequest:request];

   return [self Requesthttp:request];
    
}


- (KHWebView*)Requesthttp:(NSMutableURLRequest*)request
{
    KHWebView* webview = [[KHWebView alloc]initWithFrame:self.frame];
    dispatch_queue_t queue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^{
        
        [webview loadRequest:request];
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self  addSubview:webview];
//            [self bringSubviewToFront: self.titleview];
//            [self bringSubviewToFront:self.mainbutton];
            
        });
        
        
    });
    
    return webview;
    
}



- (void) willchangeOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
}

- (void)didchangeOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if (fromInterfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
        fromInterfaceOrientation == UIInterfaceOrientationLandscapeRight )
    {
        
        
    }else{
        
    }
    
    
    [self initframe];
    
}




- (void) EdgeLeftPanDetected:(UIScreenEdgePanGestureRecognizer*)gesture
{
    if ([[self.CurWebView subviews] count] <  2) {
        return;
    }

    if (gesture.state == UIGestureRecognizerStateBegan) {
      
//        UIWebView* webview = [[self.CurWebView subviews]lastObject];
//
//        KHView* slidingbackview = [[KHView alloc]init];
//        slidingbackview.backgroundColor = [UIColor blackColor];
//        slidingbackview.alpha = 0.3;
//        slidingbackview.frame = self.CurWebView.frame;
//        slidingbackview.tag = 101;
//        [self.CurWebView insertSubview:slidingbackview atIndex:[[self.CurWebView subviews] count]-1];
//       
//        KHView* sideview = [[ KHView alloc ] init];
//        sideview.tag = 100;
//        sideview.frame = CGRectMake(-3, 0, 3,  webview.frame.size.height);
//        sideview.backgroundColor = [UIColor blackColor];
//        sideview.alpha = 0.2;
//        [webview addSubview:sideview];
//
//        [self.CurWebView bringSubviewToFront:[[self.CurWebView subviews]lastObject]];
        
  
    }else  if (gesture.state == UIGestureRecognizerStateChanged) {
        UIWebView* webview = [[self.CurWebView subviews]lastObject];
        webview.frame = CGRectMake([ gesture locationInView:webview.superview ].x, webview.frame.origin.y, webview.frame.size.width, webview.frame.size.height);

    }else  if (gesture.state == UIGestureRecognizerStateEnded ||
               gesture.state == UIGestureRecognizerStateCancelled)
    {

//        UIWebView* webview = [[self.CurWebView subviews]lastObject];
//        CGPoint translation = [gesture translationInView:gesture.view];
//        if (translation.x > self.frame.size.width/2) {
//            [UIView animateWithDuration:0.2 animations:^{
//                webview.frame = CGRectMake( webview.frame.size.width, webview.frame.origin.y, webview.frame.size.width, webview.frame.size.height);
//            } completion:^(BOOL finished) {
//                webview.frame = CGRectMake(0, self.frame.origin.y, webview.frame.size.width, webview.frame.size.height);
//
//                KHView* slidingbackview = ( KHView*)[self.CurWebView viewWithTag:101];
//                [slidingbackview removeFromSuperview];
//            
//                UIWebView* wb = webview ;
//                
//                [self.arryForwordViews addObject:wb];
//                
//                [webview removeFromSuperview];
//                
//
//                
//                
//            }];
//
//        }else
        {
//            UIWebView* webview = [[self.CurWebView subviews]lastObject];
//            
//            [UIView animateWithDuration:0.2 animations:^{
//               webview.frame = CGRectMake(0, webview.frame.origin.y, webview.frame.size.width, webview.frame.size.height);
//            } completion:^(BOOL finished) {
//                webview.frame = CGRectMake(0, self.frame.origin.y, webview.frame.size.width, webview.frame.size.height);
//                
//                KHView* sideview = ( KHView*)[webview viewWithTag:100];
//                [sideview removeFromSuperview];
//                
//                KHView* slidingbackview = ( KHView*)[self.CurWebView viewWithTag:101];
//                [slidingbackview removeFromSuperview];
//
//            }];

        }




    }

}

- (void) EdgeRightPanDetected:(UIScreenEdgePanGestureRecognizer*)gesture {

    if ([self.arryForwordViews count] < 1) {
        return;
    }

    if (gesture.state == UIGestureRecognizerStateBegan) {
        
        

    }else if (gesture.state == UIGestureRecognizerStateChanged) {
        
        UIWebView* webview = (UIWebView*)[self.arryForwordViews lastObject];
        webview.frame = CGRectMake([ gesture locationInView:webview.superview ].x, webview.frame.origin.y, webview.frame.size.width, webview.frame.size.height);

    }else if (gesture.state == UIGestureRecognizerStateEnded ||
              gesture.state == UIGestureRecognizerStateCancelled) {


        UIWebView* webview = (UIWebView*)[self.arryForwordViews lastObject];
        
        CGPoint translation = [gesture translationInView:gesture.view];
        NSLog(@"%@",[NSString stringWithFormat:@"(%0.0f, %0.0f)", translation.x, translation.y]);

        if ( translation.x*-1 > self.frame.size.width/2)
        {

            [UIView animateWithDuration:0.2 animations:^{
                webview.frame = CGRectMake( 0, webview.frame.origin.y, webview.frame.size.width, webview.frame.size.height);
            } completion:^(BOOL finished) {
   
                [self.CurWebView addSubview:[webview copy]];
                [self.arryForwordViews removeObject:webview];
                
            }];

        }else
        {

            [UIView animateWithDuration:0.2 animations:^{
                webview.frame = CGRectMake(  webview.frame.size.width, webview.frame.origin.y, webview.frame.size.width, webview.frame.size.height);
            } completion:^(BOOL finished) {
              
            }];

        }

    }

}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


@end
