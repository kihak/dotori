//
//  MainViewController.m
//  dotori
//
//  Created by kihak on 2014. 5. 13..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import "MainViewController.h"
#import "KHUIWebViewController.h"
#import "shareDataManager.h"
#import "WebSiteTableViewController.h"
#import "DraggableCollectionViewFlowLayout.h"
#import "AddSiteViewController.h"
#import "ChideWebViewController.h"

#import "APLCollectionViewCell.h"
#import "INCCustomContainer.h"


@interface MainViewController()
@end

@implementation MainViewController
{

    NSMutableArray* _data;
    
}

- (UIColor *)aapl_applicationGreenColor {
    return [UIColor colorWithRed:0.255 green:0.804 blue:0.470 alpha:1];
}

- (UIColor *)aapl_applicationBlueColor {
    return [UIColor colorWithRed:0.333 green:0.784 blue:1 alpha:1];
}

- (UIColor *)aapl_applicationPurpleColor {
    return [UIColor colorWithRed:0.659 green:0.271 blue:0.988 alpha:1];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _data = [[shareDataManager Instance].arryuserdata  mutableCopy];
    
//    [_data addObject:@{JSON_KEY_TYPE:@"2", JSON_KEY_IMAGE_NAME:@"",JSON_KEY_URL:@"",JSON_KEY_NAME:@"'",JSON_KEY_IMAGE:@"'"}
//    ];
//

    DraggableCollectionViewFlowLayout *grid = [[DraggableCollectionViewFlowLayout alloc] init];
    grid.itemSize = CGSizeMake(75.0, 75.0);
    grid.sectionInset = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
    
    _collectionView = [[UICollectionView alloc]initWithFrame:((INCCustomContainer *)self.customContainer).viewBounce collectionViewLayout:grid];
    [_collectionView registerClass:[APLCollectionViewCell class] forCellWithReuseIdentifier:@"CELL_ID"];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.draggable = YES;
    _collectionView.scrollEnabled = YES;
    [self.view addSubview:_collectionView];
    
}

- (void)gomenu:(id)sender
{
    AddSiteViewController* addsiteviewcontroller =  [[AddSiteViewController alloc]init];
    [self.navigationController pushViewController:addsiteviewcontroller animated:NO];
    

}
-(void)closedMenu
{
    [self.customContainer closedMenu];
}
- (void)action:(id)sender
{
	NSLog(@"-[%@ %@], Selected segment is: %zi", [self class], NSStringFromSelector(_cmd), [sender selectedSegmentIndex]);

    switch ([sender selectedSegmentIndex]) {
        case 0:
        {
        }
            break;
        case 1:
        {
            WebSiteTableViewController* webtablecontroller = [[WebSiteTableViewController alloc]init];
            [self.customContainer pushViewController:webtablecontroller animated:YES];
            
          }
            break;
        case 2:
            
            break;
            
        default:
            break;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath

{
    CGSize size = CGSizeMake(90, 130);
    return size;
}


//
//- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
//{
//    return   1;
//}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_data count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    APLCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL_ID" forIndexPath:indexPath];
  
    NSDictionary* dic =   [_data objectAtIndex:indexPath.row];
    cell.cellType = [dic objectForKey:JSON_KEY_TYPE] ;
 
    if ([cell.cellType  isEqualToString:@"1"])
    {
        [cell.titleLabel setText: [dic objectForKey:JSON_KEY_NAME]];
        [cell.subTitleLabel setText: [dic objectForKey:JSON_KEY_URL]];
        [cell.imageView setImage:[shareDataManager loadImagefile:[dic objectForKey:JSON_KEY_IMAGE_NAME]]];
    }
    
    
    
    [cell reflash];
    
    return cell;
}

- (BOOL)collectionView:(LSCollectionViewHelper *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    // Prevent item from being moved to index 0
    //    if (toIndexPath.item == 0) {
    //        return NO;
    //    }
    return YES;
}

- (void)collectionView:(LSCollectionViewHelper *)collectionView moveItemAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSDictionary *data1 = [_data objectAtIndex:fromIndexPath.item];
    NSDictionary *index = [data1 mutableCopy];
    
    [_data removeObject:data1];
    [_data insertObject:index atIndex:toIndexPath.item];

}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // used tapped a collection view cell, navigate to a detail view controller showing that single photo
    APLCollectionViewCell *cell = (APLCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if ([cell.cellType isEqualToString:@"1"])
    {
        KHUIWebViewController* webviewcontroller =  [[KHUIWebViewController alloc]init];
        webviewcontroller.view.frame = [self.customContainer viewBounce];
        [webviewcontroller changewebview];
 #if !TARGET_IPHONE_SIMULATOR
        [webviewcontroller loadurl:cell.subTitleLabel.text index:indexPath.row];
#else
        [webviewcontroller loadurl:cell.subTitleLabel.text index:indexPath.row] ;
#endif
        
        
        [self.customContainer pushViewController:webviewcontroller animated:NO];

    }else
    {
        [self Addaddress];
        
    }
}

- (void)Addaddress
{
    AddSiteViewController* addsiteviewcontroller =  [[AddSiteViewController alloc]init];
    addsiteviewcontroller.view.frame =[self.customContainer viewBounce];
    [addsiteviewcontroller changewebview];
    [self.customContainer pushViewController:addsiteviewcontroller animated:NO];

}

- (UIBarButtonItem *)addBarButtonItem {
    if (!_addBarButtonItem) {
        _addBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                           target:self
                                                                           action:@selector(addClicked:)];
		_addBarButtonItem.width = 18.0f;
    }
    return _addBarButtonItem;
}



- (UIBarButtonItem *)editBarButtonItem {
    if (!_editBarButtonItem) {
        _editBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                             target:self
                                                             action:@selector(editClicked:)];
		_editBarButtonItem.width = 18.0f;
    }
    return _editBarButtonItem;
}


-(void)addClicked:(id)sender
{
    ChideWebViewController* childwebcontroller = [[ChideWebViewController alloc]init];
    [self.customContainer pushViewController:childwebcontroller animated:YES];
    childwebcontroller.view.frame =[self.customContainer viewBounce];
    [childwebcontroller showViewMenu];
  
}


-(void)editClicked:(id)sender
{
    
    WebSiteTableViewController* webtablecontroller = [[WebSiteTableViewController alloc]init];
    webtablecontroller.view.frame =[self.customContainer viewBounce];
    [self.customContainer pushViewController:webtablecontroller animated:YES];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    _data = [[shareDataManager Instance].arryuserdata  mutableCopy];
    
    [_data addObject:@{JSON_KEY_TYPE:@"2", JSON_KEY_IMAGE_NAME:@"",JSON_KEY_URL:@"",JSON_KEY_NAME:@"'",JSON_KEY_IMAGE:@"'"}
     ];
    
    [_collectionView reloadData];

    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *items = [NSArray arrayWithObjects:
                      fixedSpace,
                      self.editBarButtonItem,
                      flexibleSpace,
                      self.addBarButtonItem,
                      flexibleSpace,
                      flexibleSpace,
                      flexibleSpace,
                      flexibleSpace,
                      fixedSpace,
                      nil];
    self.customContainer.bottomBar.items = items;

    [super viewWillAppear:animated];
}


-(void)reflash
{
}

-(void)addWebViewController:(NSURLRequest *)request frame:(CGRect)frame
{
   
    KHUIWebViewController* webviewcontroller =  [[KHUIWebViewController alloc]init];
    webviewcontroller.view.frame = frame;
    [webviewcontroller changewebview];
    
    [webviewcontroller loadrequest:request];
    [self.customContainer  pushViewController:webviewcontroller animated:NO];
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)tointerfaceOrientation duration:(NSTimeInterval)duration
{
    _collectionView.frame = self.view.frame;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{

    _collectionView.frame = self.view.frame;
    
}



@end
