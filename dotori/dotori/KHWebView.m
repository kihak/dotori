//
//  KHWebView.m
//  kafari
//
//  Created by kihak on 2014. 4. 15..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import "KHWebView.h"

NSString *completeRPCURL2 = @"webviewprogressproxy:///complete";

@implementation KHWebView

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        // copy the relevant features of the current instance to the copy instance
    }
    return copy;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.frame = frame;
        
        self.mainwebview = [[UIWebView alloc]initWithFrame:frame];
        self.mainwebview.delegate = self;
        [self addSubview:self.mainwebview];
        
  
        self.imageCapture = [[UIImage alloc]init];
       

        
    }
    return self;
}

- (void)chageFont:(int)textSize
{
    NSString * jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'", textSize];
    [self.mainwebview stringByEvaluatingJavaScriptFromString:jsString];

}

- (void)dealloc
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void)loadRequest:(NSURLRequest *)request
{
    [self.mainwebview loadRequest:request];
}

- (void)loadHTMLString:(NSString *)string baseURL:(NSURL *)baseURL
{
    [self.mainwebview loadHTMLString:string baseURL:baseURL];
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([request.URL.absoluteString isEqualToString:completeRPCURL2]) {
       // [self completeProgress];
        return NO;
    }
    BOOL isFragmentJump = NO;
    if (request.URL.fragment) {
        NSString *nonFragmentURL = [request.URL.absoluteString stringByReplacingOccurrencesOfString:[@"#" stringByAppendingString:request.URL.fragment] withString:@""];
        isFragmentJump = [nonFragmentURL isEqualToString:webView.request.URL.absoluteString];
    }
    
    BOOL isTopLevelNavigation = [request.mainDocumentURL isEqual:request.URL];
    BOOL ret = YES;
   
    BOOL isHTTP = [request.URL.scheme isEqualToString:@"http"] || [request.URL.scheme isEqualToString:@"https"];
    if (ret && !isFragmentJump && isHTTP && isTopLevelNavigation) {
        _currentURL = request.URL;
    //    [self reset];
    }

    switch (navigationType)
    {

        case UIWebViewNavigationTypeLinkClicked:
        {
            UIWebView* webview = [[UIWebView alloc]initWithFrame:self.frame];
            webview.delegate = self;
            [self addSubview:webview];
            [webview loadRequest:request];
            return false;

        }
            break;
    
        case UIWebViewNavigationTypeFormSubmitted:
            break;
        case UIWebViewNavigationTypeBackForward:
            break;

        case UIWebViewNavigationTypeReload:
            break;

        case UIWebViewNavigationTypeFormResubmitted:
            break;
        case UIWebViewNavigationTypeOther:
            
            break;
            
        default:
            break;
    }
    
    return TRUE;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
 //   self.imageCapture  = [UIImage imageWithView:self.mainwebview];
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    
    
    NSString *readyState = [webView stringByEvaluatingJavaScriptFromString:@"document.readyState"];
    
    BOOL interactive = [readyState isEqualToString:@"interactive"];
    if (interactive) {
        _interactive = YES;
        NSString *waitForCompleteJS = [NSString stringWithFormat:@"window.addEventListener('load',function() { var iframe = document.createElement('iframe'); iframe.style.display = 'none'; iframe.src = '%@'; document.body.appendChild(iframe);  }, false);", completeRPCURL2];
        [webView stringByEvaluatingJavaScriptFromString:waitForCompleteJS];
    }
    
    BOOL isNotRedirect = _currentURL && [_currentURL isEqual:webView.request.mainDocumentURL];
    BOOL complete = [readyState isEqualToString:@"complete"];
    if (complete && isNotRedirect) {
       // [self completeProgress];
    }
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSString *readyState = [webView stringByEvaluatingJavaScriptFromString:@"document.readyState"];
    
    BOOL interactive = [readyState isEqualToString:@"interactive"];
    if (interactive) {
        _interactive = YES;
        NSString *waitForCompleteJS = [NSString stringWithFormat:@"window.addEventListener('load',function() { var iframe = document.createElement('iframe'); iframe.style.display = 'none'; iframe.src = '%@'; document.body.appendChild(iframe);  }, false);", completeRPCURL2];
        [webView stringByEvaluatingJavaScriptFromString:waitForCompleteJS];
    }
    
    BOOL isNotRedirect = _currentURL && [_currentURL isEqual:webView.request.mainDocumentURL];
    BOOL complete = [readyState isEqualToString:@"complete"];
    if (complete && isNotRedirect) {
      //  [self completeProgress];
    }

}

- (BOOL)canGoBack
{
    return [self.mainwebview canGoBack];
}
- (BOOL)canGoForward
{
    return [self.mainwebview canGoForward];
}

- (void)goBack
{
    [self.mainwebview goBack];
}
- (void)goForward
{
    [self.mainwebview goForward];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
