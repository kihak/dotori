//
//  defines.h
//  dotori
//
//  Created by kkh on 2014. 5. 29..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#ifndef dotori_defines_h
#define dotori_defines_h


#define JSON_KEY_TYPE           @"Type"
#define JSON_KEY_NAME           @"Name"
#define JSON_KEY_URL            @"Url"
#define JSON_KEY_IMAGE          @"Image"
#define JSON_KEY_IMAGE_NAME     @"filename"
#define JSON_KEY_DATA           @"data"


#define JSON_KEY_TAB_INDEX      @"Index"
#define JSON_KEY_TAB_URL        @"Index"



#define PATH_FILE_IMAGE  @"images"
#define PATH_FILE_DATA   @"data"

#define FILE_JSON_DEFAULT        @"sites.json"
#define FILE_JSON_OPENWINDOWS    @"openwindow.json"

#endif
