//
//  KHViewController.m
//  dotori
//
//  Created by kkh on 2014. 5. 29..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import "KHViewController.h"
#import "INCCustomContainer.h"


@interface KHViewController ()

@end

@implementation KHViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor blackColor]];
//   [self.view setBackgroundColor:[UIColor colorWithRed:drand48() green:drand48() blue:drand48() alpha:1.0]];
}


@end
