//
//  KHUIWebViewController.m
//  dotori
//
//  Created by kkh on 2014. 5. 16..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import "KHUIWebViewController.h"
#import "NJKWebViewProgressView.h"
#import "shareDataManager.h"
#import "INCCustomContainer.h"
#import "SVWebViewControllerActivityChrome.h"
#import "SVWebViewControllerActivitySafari.h"
#import "WebSiteTableViewController.h"



@implementation NSURL (DictionaryValue)
-(NSDictionary *)dictionaryValue
{
    NSString *string =  [[self.absoluteString stringByReplacingOccurrencesOfString:@"+" withString:@" "]
                         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSScanner *scanner = [NSScanner scannerWithString:string];
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"&?"]];
    
    NSString *temp,*url;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [scanner scanUpToString:@"?" intoString:&url];       //ignore the beginning of the string and skip to the vars
    while ([scanner scanUpToString:@"&" intoString:&temp])
    {
        NSArray *parts = [temp componentsSeparatedByString:@"="];
        if([parts count] == 2)
        {
            [dict setObject:[parts objectAtIndex:1] forKey:[parts objectAtIndex:0]];
        }
    }
    
    return dict;
}
@end


@interface KHUIWebViewController ()
{
    NJKWebViewProgressView *_progressView;
    NJKWebViewProgress *_progressProxy;

}
@end


@implementation KHUIWebViewController
{
    UIWebView *_webView;
    NSString  *_urlHost;
    NSString  *_url;
    NSString  *_urlfirst;
    int        _nindex;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

     }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        _data = [[aDecoder decodeObjectForKey:@"data"] copy];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.data forKey:@"data"];
}


- (void)addsubwebview
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _progressProxy = [[NJKWebViewProgress alloc] init];
    _webView = [[UIWebView alloc]init];
    [_webView setFrame:CGRectZero];
    [self.view addSubview:_webView];
    _webView.scrollView.delegate = self;
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.0f;
    CGRect barFrame = CGRectMake(0,  ((INCCustomContainer *)self.customContainer).topBar.frame.size.height - progressBarHeight,  ((INCCustomContainer *)self.customContainer).topBar.frame.size.width, progressBarHeight);
    _progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    
    _nindex = -1;

  }


- (UIBarButtonItem *)listBarButtonItem {
    if (!_listBarButtonItem) {
        _listBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks
                                                             target:self
                                                             action:@selector(goListClicked:)];
		_listBarButtonItem.width = 18.0f;
    }
    return _listBarButtonItem;
}



- (UIBarButtonItem *)backBarButtonItem {
    if (!_backBarButtonItem) {
        _backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"SVWebViewController.bundle/SVWebViewControllerBack"]
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:@selector(goBackClicked:)];
		_backBarButtonItem.width = 18.0f;
    }
    return _backBarButtonItem;
}

- (UIBarButtonItem *)forwardBarButtonItem {
    if (!_forwardBarButtonItem) {
        _forwardBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"SVWebViewController.bundle/SVWebViewControllerNext"]
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(goForwardClicked:)];
		_forwardBarButtonItem.width = 18.0f;
    }
    return _forwardBarButtonItem;
}

- (UIBarButtonItem *)refreshBarButtonItem {
    if (!_refreshBarButtonItem) {
        _refreshBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reloadClicked:)];
    }
    return _refreshBarButtonItem;
}

- (UIBarButtonItem *)stopBarButtonItem {
    if (!_stopBarButtonItem) {
        _stopBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(stopClicked:)];
    }
    return _stopBarButtonItem;
}

- (UIBarButtonItem *)actionBarButtonItem {
    if (!_actionBarButtonItem) {
        _actionBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionButtonClicked:)];
    }
    return _actionBarButtonItem;
}

#pragma mark - Target actions

- (void)goBackClicked:(UIBarButtonItem *)sender {
    [_webView goBack];
}

- (void)goForwardClicked:(UIBarButtonItem *)sender {
    [_webView goForward];
}

- (void)reloadClicked:(UIBarButtonItem *)sender {
    [_webView reload];
}

- (void)stopClicked:(UIBarButtonItem *)sender {
    [_webView stopLoading];
	[self updateToolbarItems];
}

- (void)goListClicked:(UIBarButtonItem *)sender {

    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: nil
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: nil];
    
    for (NSDictionary* dic in [shareDataManager Instance].arryuserdata)
    {
        [actionSheet addButtonWithTitle:  [dic objectForKey:JSON_KEY_NAME]];
    }
    
    [actionSheet addButtonWithTitle: @"Edit"];
    [actionSheet addButtonWithTitle: @"Cancel"];
    [actionSheet setCancelButtonIndex: [[shareDataManager Instance].arryuserdata  count]+1];
    [actionSheet showInView:self.view];

    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( [[shareDataManager Instance].arryuserdata count] > buttonIndex) {
       
        NSDictionary* dic =   [[shareDataManager Instance].arryuserdata objectAtIndex:buttonIndex];
      [self loadurl:[dic objectForKey:JSON_KEY_URL] index:buttonIndex];
        
    }
  
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Edit"]) {
        WebSiteTableViewController* webtablecontroller = [[WebSiteTableViewController alloc]init];
        webtablecontroller.view.frame =[self.customContainer viewBounce];
        [self.customContainer pushViewController:webtablecontroller animated:YES];

    }
    
    NSLog(@"You have pressed the %@ button %d", [actionSheet buttonTitleAtIndex:buttonIndex],buttonIndex);
}

- (void)actionButtonClicked:(id)sender {
    NSArray *activities = @[[SVWebViewControllerActivitySafari new], [SVWebViewControllerActivityChrome new]];
    NSURL *url = _webView.request.URL ? _webView.request.URL : self.URL;
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:@[url] applicationActivities:activities];
    [self presentViewController:activityController animated:YES completion:nil];
}

- (void)doneButtonClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Toolbar

- (void)updateToolbarItems {
    self.backBarButtonItem.enabled = _webView.canGoBack;
    self.forwardBarButtonItem.enabled =_webView.canGoForward;
    self.actionBarButtonItem.enabled = !_webView.isLoading;
    
    if (_webView.canGoBack) {
        [self.customContainer  panGestureEnable:false];
    }else
        [self.customContainer  panGestureEnable:true];
    
    
    UIBarButtonItem *refreshStopBarButtonItem = _webView.isLoading ? self.stopBarButtonItem : self.refreshBarButtonItem;
    
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGFloat toolbarWidth = 250.0f;
        fixedSpace.width = 35.0f;
        
        NSArray *items = [NSArray arrayWithObjects:
                          fixedSpace,
                          refreshStopBarButtonItem,
                          fixedSpace,
                          self.backBarButtonItem,
                          fixedSpace,
                          self.forwardBarButtonItem,
                          fixedSpace,
                          self.actionBarButtonItem,
                          nil];
        
//        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, toolbarWidth, 44.0f)];
        ((INCCustomContainer *)self.customContainer).bottomBar.items = items;
//        toolbar.barStyle = self.navigationController.navigationBar.barStyle;
//        toolbar.tintColor = self.navigationController.navigationBar.tintColor;
   //     self.navigationItem.rightBarButtonItems = items.reverseObjectEnumerator.allObjects;
    }
    
    else {
        NSArray *items = [NSArray arrayWithObjects:
                          fixedSpace,
                          self.backBarButtonItem,
                          flexibleSpace,
                          self.forwardBarButtonItem,
                          flexibleSpace,
                          refreshStopBarButtonItem,
                          flexibleSpace,
                          self.listBarButtonItem,
                          flexibleSpace,  
                          self.actionBarButtonItem,
                          fixedSpace,
                          nil];
        
//        self.navigationController.toolbar.barStyle = self.navigationController.navigationBar.barStyle;
//        self.navigationController.toolbar.tintColor = self.navigationController.navigationBar.tintColor;
        if ( self.customContainer) {
            ((INCCustomContainer *)self.customContainer).bottomBar.items = items;
            
        }
    }
}



- (void)handleBack:(id)sender
{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self changewebview];

    CGFloat progressBarHeight = 2.0f;
    CGRect barFrame = CGRectMake(0, ((INCCustomContainer *)self.customContainer).topBar.frame.size.height - progressBarHeight, ((INCCustomContainer *)self.customContainer).topBar.frame.size.width, progressBarHeight);
    [_progressView setFrame:barFrame];

    [((INCCustomContainer *)self.customContainer).topBar addSubview:_progressView];

    [self updateToolbarItems];

//    if ([[self.view subviews] count] > 0) {
//        [self.customContainer  panGestureEnable:false];
//    }else
//        [self.customContainer  panGestureEnable:true];

    
}



- (void)oneFingerSwiperight:(UISwipeGestureRecognizer *)recognizer
{
    NSLog(@"You swiped Right");
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Remove progress view
    // because UINavigationBar is shared with other ViewControllers
    [_progressView removeFromSuperview];
}

- (void)navigationController:(UINavigationController *)navigationController
       didShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
    if ([navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        // disable interactivePopGestureRecognizer in the rootViewController of navigationController
        if ([[navigationController.viewControllers firstObject] isEqual:viewController]) {
            navigationController.interactivePopGestureRecognizer.enabled = NO;
        } else {
            // enable interactivePopGestureRecognizer
            navigationController.interactivePopGestureRecognizer.enabled = YES;
        }
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
    
}


-(void)loadrequest:(NSURLRequest *)request
{
//    static NSString *regexp = @"^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9-]*[a-zA-Z0-9])[.])+([A-Za-z]|[A-Za-z][A-Za-z0-9-]*[A-Za-z0-9])$";
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regexp];
//    
//    NSLog(@"absolute string: %@", request.URL.absoluteString);
//    NSLog(@"path: %@", request.URL.path);
//    if ([predicate evaluateWithObject:request.URL.host])
    {
        self.URL = request.URL;
        _urlHost = request.URL.host;
        [_webView loadRequest:request];
       //  [_titleLabel sizeToFit];
    }
    
   
}

-(void)loadurl:(NSString*)url index:(int)nindex
{
    _nindex = nindex;
    _urlfirst = url;
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [self loadrequest:req];
    
    
}

- (BOOL)webViewshouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
   //     [self addWebViewController:request];
     
//        KHUIWebViewController* webviewcontroller =  [[KHUIWebViewController alloc]init];
//        webviewcontroller.view.frame = self.view.frame;
//        [webviewcontroller changewebview];
//        
//        [webviewcontroller loadrequest:request];
//        [self.customContainer  pushViewController:webviewcontroller animated:NO];

            //        return true;
    }

    return false;
}


-(void)addWebViewController:(NSURLRequest *)request
{
    [[shareDataManager Instance].mainViewController  addWebViewController:request frame:self.view.frame];
}

-(void) setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners withColor:  (UIColor*) color
{
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds  byRoundingCorners:corners cornerRadii:CGSizeMake(109.0, 109.0)];
    
    CAShapeLayer* shape = [[CAShapeLayer alloc] init] ;
    [shape setPath:rounded.CGPath];
    shape.strokeColor = [[UIColor grayColor] CGColor];
    
    view.backgroundColor=color;
    view.layer.mask = shape;
}


-(NSString*)favIconUrlStringFromHtmlString:(NSString*)htmlString
{
    
	NSScanner *htmlScanner = [NSScanner scannerWithString:htmlString];
  	while ([htmlScanner isAtEnd] == NO)
  	{
        
		[htmlScanner scanUpToString:@"<link" intoString:NULL];
  		if(![htmlScanner isAtEnd])
  		{
            
			NSString *linkString;
  			[htmlScanner scanUpToString:@"/>" intoString:&linkString];
  			// we have a link element.  does it have rel set to anything we want?
            
            
            
			if(([linkString rangeOfString:@"rel=\"shortcut icon\""].location != NSNotFound)||
               ([linkString rangeOfString:@"rel=\"apple-touch-icon\""].location != NSNotFound)||
			   ([linkString rangeOfString:@"rel='apple-touch-icon'"].location != NSNotFound)||
			   ([linkString rangeOfString:@"rel='shortcut icon'"].location != NSNotFound)||
			   ([linkString rangeOfString:@"rel=\"icon\""].location != NSNotFound)||
			   ([linkString rangeOfString:@"rel='icon'"].location != NSNotFound)||
			   ([linkString rangeOfString:@"rel=icon "].location != NSNotFound))
			{
                

                
				NSScanner *hrefScanner = [NSScanner scannerWithString:linkString];
				[hrefScanner scanUpToString:@"href=" intoString:NULL];
				if(![hrefScanner isAtEnd])
				{
					[hrefScanner scanString:@"href=" intoString:NULL];
					NSString *hrefString;
					// if we don't hit one cause the href was the last thing in the element, we don't care
					[hrefScanner scanUpToString:@" " intoString:&hrefString];
                    
                
                    NSArray* arrset = @[@".png\"",@".jpg\"",@".ico\"",@".gif\""];
                    NSRange range;
                    for (NSString* set in arrset) {
                        if ([hrefString rangeOfString:set].location != NSNotFound) {
                            range = [hrefString rangeOfString:set];
                            break;
                        }
                    }
			
                    hrefString = [hrefString substringToIndex:(range.location + range.length)];
                    
                    // clean up any quotes
					hrefString = [hrefString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
					hrefString = [hrefString stringByReplacingOccurrencesOfString:@"'" withString:@""];
					hrefString = [hrefString stringByReplacingOccurrencesOfString:@">" withString:@""];
  					// we're done, return
					
                    if ([[hrefString substringWithRange:NSMakeRange(0,1)] isEqualToString:@"/"]) {
                        
                        hrefString= [_urlfirst stringByAppendingString:hrefString];
                        
                    }
                    
                    
                    return hrefString;
                    
				}
			}
		}
	}
	return nil;
}

- (void)webViewLoadcomplete
{
//    NSString* strtitle = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
//    [_subTitleLabel setText:_urlHost];
//    [_titleLabel setText:strtitle];
//    [self resizetitle];
//    NSString *htmlString = @"HTML String";
//    [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementById('elementid').innerHTML = \"%@\";", htmlString]];
    
    if (_nindex >= 0)
    {
        
        NSString *html = [_webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
        NSString *shortcuturl = [self  favIconUrlStringFromHtmlString:html];
  
        time_t tm;
        localtime(&tm);
        NSString* strImageName = [NSString stringWithFormat:@"%2ld_", tm ];
        if (shortcuturl != nil) {
            NSArray* arry = [shortcuturl componentsSeparatedByString:@"/"];
            strImageName = [strImageName stringByAppendingString:[arry lastObject]];
            NSDictionary* dic = [[shareDataManager Instance].arryuserdata objectAtIndex:_nindex];
            [[shareDataManager Instance].arryuserdata replaceObjectAtIndex:_nindex withObject:  @{JSON_KEY_TYPE:@"1", JSON_KEY_IMAGE_NAME:strImageName,JSON_KEY_URL:_urlfirst,JSON_KEY_NAME:[dic objectForKey:JSON_KEY_NAME],JSON_KEY_IMAGE:shortcuturl}];
            _nindex = -1;
            
            [shareDataManager  saveImageFileUrl:shortcuturl filename:strImageName];
            [shareDataManager saveJsonfile:FILE_JSON_DEFAULT];
        }
    
    }
    
      [self updateToolbarItems];
}


#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
 
   ((INCCustomContainer *)self.customContainer).titleLabel.text =  _urlHost;//[_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    [_progressView setProgress:progress animated:YES];

}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)tointerfaceOrientation duration:(NSTimeInterval)duration
{
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self changewebview];
}

-(void)changewebview
{
    [_webView setFrame:self.view.bounds];
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    CGFloat height = self.navigationController.navigationBar.frame.size.height;
//    CGFloat y = scrollView.bounds.origin.y;
//    if (y <= 0) {
//        CGRect frame =  self.navigationController.navigationBar.frame;
//        frame.origin.y = 0;
//         self.navigationController.navigationBar.frame = frame;
//    } else if (_webView.scrollView.contentSize.height > _webView.frame.size.height) {
//        CGFloat diff = height - y;
//        CGRect frame =  self.navigationController.navigationBar.frame;
//        frame.origin.y = -y;
//        if (frame.size.height < 20) {
//            frame.origin.y = 0;
//            frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 10);
//        }
//         self.navigationController.navigationBar.frame = frame;
//        
//        CGFloat origin = 0;
//        CGFloat h = height; // height of the tableHeaderView
////        if (diff > 0) {
////            origin = diff;
////            h = y;
////        }
//        frame = _webView.frame;
//        frame.origin.y = origin;
//        frame.size.height = _webView.superview.frame.size.height - origin;
//        _webView.frame = frame;
//        
//
//    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
