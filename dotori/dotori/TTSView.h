//
//  TTSView.h
//  kafari
//
//  Created by kihak on 2014. 4. 9..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import <UIKit/UIKit.h>
@import AVFoundation.AVSpeechSynthesis;

@interface TTSView : UIView<AVSpeechSynthesizerDelegate>
@property (strong, nonatomic) NSArray *languageCodes;
@property (strong, nonatomic) NSDictionary *languageDictionary;
@property (strong, nonatomic) AVSpeechSynthesizer *synthesizer;
@property (nonatomic)NSAttributedString* ttstext;
@property (nonatomic)UITextView *TextView;
@end
