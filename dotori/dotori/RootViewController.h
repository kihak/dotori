//
//  RootViewController.h
//  kafari
//
//  Created by kihakkim on 2014. 4. 7..
//  Copyright (c) 2014년 smartkt. All rights reserved.
//

#import <UIKit/UIKit.h>

@import CoreTelephony.CTTelephonyNetworkInfo; // new modules syntax!


@interface RootViewController : UIViewController


-(void) chageview:(int)ntype;
@property (nonatomic, strong) CTTelephonyNetworkInfo *networkInfo;

@end
