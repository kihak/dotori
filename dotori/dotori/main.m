//
//  main.m
//  dotori
//
//  Created by kihak on 2014. 5. 13..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
