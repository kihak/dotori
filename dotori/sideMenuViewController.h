//
//  sideMenuViewController.h
//  dotori
//
//  Created by kkh on 2014. 6. 11..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICollectionView+Draggable.h"


@interface sideMenuViewController : UIViewController<UIGestureRecognizerDelegate,UICollectionViewDataSource_Draggable, UICollectionViewDelegate>

-(void)showViewMenu;

@end
