//
//  ChideWebViewController.h
//  dotori
//
//  Created by kkh on 2014. 6. 23..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICollectionView+Draggable.h"
#import "KHViewController.h"

@interface UICopyableWebView : UIWebView <NSCopying>
@end

@interface ChideWebViewController : KHViewController<UIGestureRecognizerDelegate,UICollectionViewDataSource_Draggable, UICollectionViewDelegate>

-(void)showViewMenu;

@end
