//
//  sideMenuViewController.m
//  dotori
//
//  Created by kkh on 2014. 6. 11..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import "sideMenuViewController.h"
#import "shareDataManager.h"
#import "APLCollectionViewCell.h"
#import "DraggableCollectionViewFlowLayout.h"

@interface sideMenuViewController ()
{
    NSMutableArray* _data;
    UICollectionView* _collectionView;
}
@end

@implementation sideMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:[UIColor blackColor]];
     self.view.alpha = 0.7;
   // [self addGestureRecognizerForClosingMenu];
    
        //  _data = [[shareDataManager Instance].arryuserdata  mutableCopy];
    _data = [[NSMutableArray alloc]initWithCapacity:100];
    [_data addObject:@{JSON_KEY_TYPE:@"2", JSON_KEY_IMAGE_NAME:@"",JSON_KEY_URL:@"",JSON_KEY_NAME:@"'",JSON_KEY_IMAGE:@"'"}
     ];
    
    
    DraggableCollectionViewFlowLayout *grid = [[DraggableCollectionViewFlowLayout alloc] init];
    grid.itemSize = CGSizeMake(30.0, 30.0);
    grid.sectionInset = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
    
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:grid];
    [_collectionView registerClass:[APLCollectionViewCell class] forCellWithReuseIdentifier:@"CELL_ID"];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.draggable = YES;
    _collectionView.backgroundColor = [UIColor grayColor];
    _collectionView.scrollEnabled = YES;

    [self.view addSubview:_collectionView];

}


-(void)showViewMenu
{
    [_collectionView setFrame:self.view.bounds];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [[shareDataManager Instance].mainViewController closedMenu];

}

- (void)addGestureRecognizerForOpeningMenu {
    
    // add gesture recognizer
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    recognizer.delegate = self;
    [self.view addGestureRecognizer:recognizer];
}

- (void)addGestureRecognizerForClosingMenu {
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    recognizer.delegate = self;
    [self.view addGestureRecognizer:recognizer];
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    
    // calculate x and y coordinates for transition on gesture point
    //CGPoint translationInView = [recognizer translationInView:self.view];
    
    // handle gesture recognizer states
    switch (recognizer.state) {
            // begin
        case UIGestureRecognizerStateBegan: {
              }
           break;
            // change
        case UIGestureRecognizerStateChanged: {
        }
            break;
            // end
        case UIGestureRecognizerStateEnded: {
        
        }
            break;
        default:break;
    }
}




- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath

{
    CGSize size = CGSizeMake(30, 30);
    return size;
}


    //
    //- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
    //{
    //    return   1;
    //}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_data count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    APLCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL_ID" forIndexPath:indexPath];
    
    NSDictionary* dic =   [_data objectAtIndex:indexPath.row];
    cell.cellType = [dic objectForKey:JSON_KEY_TYPE] ;
    
    if ([cell.cellType  isEqualToString:@"2"]) {
        
        [cell.titleLabel setText: @"+"];
        [cell.subTitleLabel setText: @"add"];
        [cell.imageView setImage:nil];
        cell.cellType = @"2";
        
    }else if ([cell.cellType  isEqualToString:@"1"])
    {
        [cell.titleLabel setText: [dic objectForKey:JSON_KEY_NAME]];
        [cell.subTitleLabel setText: [dic objectForKey:JSON_KEY_URL]];
        [cell.imageView setImage:[shareDataManager loadImagefile:[dic objectForKey:JSON_KEY_IMAGE_NAME]]];
    }
    
    
    
    [cell reflash];
    
    return cell;
}

- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view
{
    return 1;
}
- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    
    return 1;
}
- (void)scrollToItemAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UICollectionViewScrollPosition)scrollPosition animated:(BOOL)animated
{
    
}

- (BOOL)collectionView:(LSCollectionViewHelper *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath toIndexPath:(NSIndexPath *)toIndexPath
{
        // Prevent item from being moved to index 0
        //    if (toIndexPath.item == 0) {
        //        return NO;
        //    }
    return YES;
}

- (void)collectionView:(LSCollectionViewHelper *)collectionView moveItemAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSDictionary *data1 = [_data objectAtIndex:fromIndexPath.item];
    NSDictionary *index = [data1 mutableCopy];
    
    [_data removeObject:data1];
    [_data insertObject:index atIndex:toIndexPath.item];
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
        // used tapped a collection view cell, navigate to a detail view controller showing that single photo
    APLCollectionViewCell *cell = (APLCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if ([cell.cellType isEqualToString:@"1"])
    {
    }else
    {
    }
    
    [[shareDataManager Instance].mainViewController closedMenu];

    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
