//
//  UIView+copying.m
//  dotori
//
//  Created by kkh on 2014. 6. 23..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import "UIView+copying.h"

@implementation UIView (copying)

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
            // copy the relevant features of the current instance to the copy instance
    }
    return copy;
}

- (id) clone {
    NSData *archivedViewData = [NSKeyedArchiver archivedDataWithRootObject: self];
    id clone = [NSKeyedUnarchiver unarchiveObjectWithData:archivedViewData];
    return clone;
}


@end
