//
//  ChideWebViewController.m
//  dotori
//
//  Created by kkh on 2014. 6. 23..
//  Copyright (c) 2014년 smartkt.com. All rights reserved.
//

#import "ChideWebViewController.h"
#import "shareDataManager.h"
#import "APLCollectionViewCell.h"
#import "DraggableCollectionViewFlowLayout.h"
#import "INCCustomContainer.h"
#import "UIView+copying.h"

@implementation UICopyableWebView
- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        // copy the relevant features of the current instance to the copy instance
    }
    return copy;
}
@end


@interface ChideWebViewController ()
{
    UICollectionView* _collectionView;

}
@end

@implementation ChideWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated    // Called when the view is about to made visible. Default does nothing
{
        //    [self.customContainer.topBar setHidden:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    DraggableCollectionViewFlowLayout *grid = [[DraggableCollectionViewFlowLayout alloc] init];
    grid.itemSize = CGSizeMake(30.0, 30.0);
    grid.sectionInset = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
    
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:grid];
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"CELL_ID"];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.draggable = YES;
    _collectionView.backgroundColor = [UIColor grayColor];
    _collectionView.scrollEnabled = YES;
    
    [self.view addSubview:_collectionView];

    
}



-(void)showViewMenu
{
    [_collectionView setFrame:self.view.bounds];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [[shareDataManager Instance].mainViewController closedMenu];
    
}

- (void)addGestureRecognizerForOpeningMenu {
    
        // add gesture recognizer
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    recognizer.delegate = self;
    [self.view addGestureRecognizer:recognizer];
}

- (void)addGestureRecognizerForClosingMenu {
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    recognizer.delegate = self;
    [self.view addGestureRecognizer:recognizer];
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    
        // calculate x and y coordinates for transition on gesture point
        //CGPoint translationInView = [recognizer translationInView:self.view];
    
        // handle gesture recognizer states
    switch (recognizer.state) {
                // begin
        case UIGestureRecognizerStateBegan: {
        }
            break;
                // change
        case UIGestureRecognizerStateChanged: {
        }
            break;
                // end
        case UIGestureRecognizerStateEnded: {
            
        }
            break;
        default:break;
    }
}




- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath

{
    CGSize size = CGSizeMake(130, 150);
    return size;
}


    //
    //- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
    //{
    //    return   1;
    //}
//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
//{
//    CATransform3D transform = ((CALayer*)self.view.layer.presentationLayer).transform;
//    CGFloat a = atan2(transform.m12, transform.m11);
//    CGPoint offsetPoint = CGPointMake(point.x - CGRectGetWidth(self.view.frame)*0.5, point.y - CGRectGetHeight(self.view.frame)*0.5);
//    CGPoint rotatedPoint = CGPointApplyAffineTransform(offsetPoint, CGAffineTransformMakeRotation(M_PI-a));
//    CGPoint inverseOffsetRotatedPoint = CGPointMake(rotatedPoint.x + CGRectGetWidth(self.view.frame)*0.5, rotatedPoint.y + CGRectGetHeight(self.view.frame)*0.5);
//    
//    
//    return [self.view hitTest:inverseOffsetRotatedPoint withEvent:event];
//}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.customContainer.webviewControllerStack count];
}

-(UIViewController*) copyOfView:(UIViewController*) viewToCopy
{
    
        // viewToCopy is the view that will be dragged and having subviews
    if([viewToCopy isKindOfClass:[UIViewController class]])
    {
        
        NSData* viewCopyData = [NSKeyedArchiver archivedDataWithRootObject:viewToCopy];
        return [NSKeyedUnarchiver unarchiveObjectWithData:viewCopyData];
        
    }
    
    /* If its not a UIView then return nil */
    
    return nil;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL_ID" forIndexPath:indexPath];
    cell.layer.borderWidth = 0;
    cell.layer.borderColor = [UIColor clearColor].CGColor;
    UIViewController *ViewController = [self.customContainer.webviewControllerStack objectAtIndex:indexPath.row];
    if (ViewController) 
    {
        UICopyableWebView* webview = [[ViewController.view subviews] lastObject];
        NSData *tempArchiveView = [NSKeyedArchiver archivedDataWithRootObject:webview];
        UICopyableWebView *lastview = [webview copyWithZone:nil];//[NSKeyedUnarchiver unarchiveObjectWithData:tempArchiveView];

        [lastview reload];
            //        UIView *lastview = [[ViewController.view subviews] lastObject] ;
        lastview.frame = ViewController.view.frame;
        lastview.center = cell.contentView.center;
        float scaleX = cell.contentView.frame.size.width/lastview.frame.size.width;
        float scaleY = cell.contentView.frame.size.height/lastview.frame.size.height;
        [lastview setTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
        [cell.contentView addSubview:lastview ];
        
    }
    
//    cell.cellType = [dic objectForKey:JSON_KEY_TYPE] ;
    
//    if ([cell.cellType  isEqualToString:@"2"]) {
//        
//        [cell.titleLabel setText: @"+"];
//        [cell.subTitleLabel setText: @"add"];
//        [cell.imageView setImage:nil];
//        cell.cellType = @"2";
//        
//    }else if ([cell.cellType  isEqualToString:@"1"])
//    {
//        [cell.titleLabel setText: [dic objectForKey:JSON_KEY_NAME]];
//        [cell.subTitleLabel setText: [dic objectForKey:JSON_KEY_URL]];
//        [cell.imageView setImage:[shareDataManager loadImagefile:[dic objectForKey:JSON_KEY_IMAGE_NAME]]];
//    }
//    
//    
//    
//    [cell reflash];
    
    return cell;
}

- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view
{
    return 1;
}
- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    
    return 1;
}
- (void)scrollToItemAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UICollectionViewScrollPosition)scrollPosition animated:(BOOL)animated
{
    
}

- (BOOL)collectionView:(LSCollectionViewHelper *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath toIndexPath:(NSIndexPath *)toIndexPath
{
        // Prevent item from being moved to index 0
        //    if (toIndexPath.item == 0) {
        //        return NO;
        //    }
    return YES;
}

- (void)collectionView:(LSCollectionViewHelper *)collectionView moveItemAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSDictionary *data1 = [self.customContainer.webviewControllerStack objectAtIndex:fromIndexPath.item];
    NSDictionary *index = [data1 mutableCopy];
    
    [self.customContainer.webviewControllerStack removeObject:data1];
    [self.customContainer.webviewControllerStack insertObject:index atIndex:toIndexPath.item];
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
        // used tapped a collection view cell, navigate to a detail view controller showing that single photo
    APLCollectionViewCell *cell = (APLCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if ([cell.cellType isEqualToString:@"1"])
    {
    }else
    {
    }
    
    [[shareDataManager Instance].mainViewController closedMenu];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
