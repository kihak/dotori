//
//  INCCustomContainer.h
//  INCCustomContainer
//
//  Created by Maximilian Kraus on 04.03.14.
//
//
//  The MIT License (MIT)
//
//  Copyright (c) 2014 Maximilian Kraus
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


#import "INCCustomContainer.h"

#import "INCCustomContainerTransition.h"
#import "sideMenuViewController.h"
#import "NBPLUtil.h"
#import "WebSiteTableViewController.h"
#import "dotori/shareDataManager.h"
#import "KHUIWebViewController.h"
#import <sys/utsname.h>


#define TOOLBAR_HEIGHT_TOP      44
#define TOOLBAR_HEIGHT_BOTTOM   44


@interface INCCustomContainer () <UIViewControllerContextTransitioning,UIGestureRecognizerDelegate>

@property (nonatomic, readwrite) UIScreenEdgePanGestureRecognizer *interactivePopGestureRecognizer;

@property (nonatomic) INCCustomContainerTransition *transitionController;




@property (nonatomic) UILabel *timeLabel;
@property (nonatomic) UILabel *batlevelLabel;
@property (nonatomic) UILabel *networkLabel;
@property (nonatomic) UIButton* menuButton;

@end

@implementation INCCustomContainer {
    
    NSMutableArray *_viewControllerStack;
    
  
    UIViewController *toViewController;
    UIViewController *fromViewController;


    UITapGestureRecognizer *_tapGestureRec;
    UIPanGestureRecognizer *_panGestureRec;

    
    
}


- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super init];
    if (self) {
        if (rootViewController) {
            _viewControllerStack = [NSMutableArray arrayWithObject:rootViewController];
        } else {
            _viewControllerStack = [NSMutableArray array];
        }
    }
    return self;
}

-(UIToolbar*)topBar
{
    if (_topBar == nil) {
       _topBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, TOOLBAR_HEIGHT_TOP)];
        [_topBar setBarStyle:UIBarStyleBlackTranslucent];
        [_topBar setBackgroundColor:[UIColor magentaColor]];
    }
    return _topBar;
}

-(UIToolbar*)bottomBar
{
    if (_bottomBar == nil) {
        _bottomBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.viewBounce.origin.y + self.viewBounce.size.height , self.viewBounce.size.width, TOOLBAR_HEIGHT_BOTTOM)];
        [_bottomBar setBarStyle:UIBarStyleBlackTranslucent];
        [_bottomBar setBackgroundColor:[UIColor magentaColor]];
    }
    return _bottomBar;
}

-(sideMenuViewController*)sidemenuViewController
{
    if (_sidemenuViewController == nil)
    {
        _sidemenuViewController = [[sideMenuViewController alloc]init];
        _panGestureRec.enabled = NO;
        [_sidemenuViewController.view setFrame:CGRectMake(0, TOOLBAR_HEIGHT_TOP , self.view.frame.size.width/2, self.viewBounce.size.height)];
        
    }
    
    return _sidemenuViewController;  
}

-(UIImageView*)rightimageView
{
    if (_rightimageView == nil) 
    {
        _rightimageView = [[UIImageView alloc]init];
        _tapGestureRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeSideBar)];
        _tapGestureRec.delegate=self;
        [_rightimageView addGestureRecognizer:_tapGestureRec];
        _tapGestureRec.enabled = NO;
        
        _panGestureRec = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveViewWithGesture:)];
        [_rightimageView addGestureRecognizer:_panGestureRec];

    }
    
    return _rightimageView;
}

-(UIView*)containerView
{
    if (_containerView == nil) {
        _containerView = [[UIView alloc] initWithFrame:[self.view bounds] ];
        _containerView.backgroundColor = [UIColor greenColor];
    }
    return _containerView;
}

-(UILabel*)titleLabel
{
    if (_titleLabel == nil) {
        _titleLabel =[[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.viewBounce.size.width, 20)];
        [_titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_titleLabel setTextColor:[UIColor magentaColor]];//[UIColor colorWithWhite:0.4 alpha:1.0]];
        [_titleLabel setFont:[UIFont systemFontOfSize:18.0]];

    }
    return _titleLabel;
}

-(UIButton*)menuButton
{
    if(_menuButton == nil)
    {
        _menuButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [_menuButton.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
        [_menuButton setTitle:@"M" forState:UIControlStateNormal];
        [_menuButton setFrame:CGRectMake(0, self.view.frame.size.height/2 - (24.0/2), 80.0, 80)];
        [_menuButton addTarget:self action:@selector(menuButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [_menuButton setBackgroundColor:[UIColor colorWithRed:100.0 green:0.0 blue:0.0 alpha:0.2]];
      
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragging:)];
        [_menuButton addGestureRecognizer:panGesture];

    }
    
    
    return _menuButton;
}

- (void)setframecontrolls
{
    self.topBar.frame = CGRectMake(0, 0, self.view.frame.size.width, TOOLBAR_HEIGHT_TOP);
    self.bottomBar.frame = CGRectMake(0, self.viewBounce.origin.y + self.viewBounce.size.height , self.viewBounce.size.width, TOOLBAR_HEIGHT_BOTTOM);
    self.containerView.frame = [self.view bounds];
    self.titleLabel.frame =CGRectMake(0, 20, self.viewBounce.size.width, 20);
    self.menuButton.frame =CGRectMake(0, self.viewBounce.size.height/2 - (24.0/2), 80.0, 80);
    self.sidemenuViewController.view.frame = CGRectMake(0, TOOLBAR_HEIGHT_TOP , self.view.frame.size.width/2, self.viewBounce.size.height);
    

}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.containerView];

    [self.view addSubview:self.rightimageView];
    self.rightimageView.userInteractionEnabled = YES;
    self.rightimageView.hidden = YES;

    [self.view addSubview:self.topBar];
    [self.view addSubview:self.bottomBar];

    [self.view addSubview:self.sidemenuViewController.view];
    self.sidemenuViewController.view.hidden = YES;

    [self.view addSubview:self.menuButton];
    [self.topBar addSubview:self.titleLabel];
    
    //  The pop recognizer
    //  Note that is is added to self.view, NOT to the containerView.
    self.interactivePopGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePopGesture:)];
    [self.interactivePopGestureRecognizer setEdges:UIRectEdgeLeft];
    [self.view addGestureRecognizer:self.interactivePopGestureRecognizer];

    
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self setframecontrolls];

}


-(void)dragging:(UIPanGestureRecognizer*)panGesture {
    
        // if is not our button, return
    if (panGesture.view != self.menuButton) {
        return;
    }
    
        // if the gesture was 'recognized'...
    if (panGesture.state == UIGestureRecognizerStateBegan || panGesture.state == UIGestureRecognizerStateChanged) {
        
            // get the change (delta)
        CGPoint delta = [panGesture translationInView:self.view];
        CGPoint center = self.menuButton.center;
        center.x += delta.x;
        center.y += delta.y;
        
            // and move the button
        self.menuButton.center = center;
        
        [panGesture setTranslation:CGPointZero inView:self.view];
    }
}



- (void)closeSideBar
{

    [self closedMenu];
//    float _LeftSCloseDuration=0.3;
//    CGAffineTransform oriT = CGAffineTransformIdentity;
//    [UIView animateWithDuration:_LeftSCloseDuration
//                     animations:^{
//                         _containerView.transform = oriT;
//                     }
//                     completion:^(BOOL finished) {
//                         _tapGestureRec.enabled = NO;
//                     }];
}

- (void)moveViewWithGesture:(UIPanGestureRecognizer *)panGes
{
    float _RightSContentOffset=160;
    float _RightSContentScale=0.85;
    float _LeftSContentOffset=160;
    float _LeftSContentScale=0.85;
    float _LeftSJudgeOffset=100;
    float _RightSJudgeOffset=100;
    static CGFloat currentTranslateX;
    
    
    if (panGes.state == UIGestureRecognizerStateBegan)
    {
        currentTranslateX = self.rightimageView.transform.tx;
    }
    if (panGes.state == UIGestureRecognizerStateChanged)
    {
        CGFloat transX = [panGes translationInView:self.rightimageView].x;
        transX = transX + currentTranslateX;
        
        CGFloat sca=0;
        [self.view sendSubviewToBack:self.sidemenuViewController.view];
        [self configureViewShadowWithDirection];
        
        if (self.rightimageView.frame.origin.x < _LeftSContentOffset)
        {
            sca = 1 - (self.rightimageView.frame.origin.x/_LeftSContentOffset) * (1-_LeftSContentScale);
        }
        else
        {
            sca = _LeftSContentScale;
        }

        CGAffineTransform transS = CGAffineTransformMakeScale(sca, sca);
        CGAffineTransform transT = CGAffineTransformMakeTranslation(transX, 0);
        
        CGAffineTransform conT = CGAffineTransformConcat(transT, transS);
        
        self.rightimageView.transform = conT;
    }
    else if (panGes.state == UIGestureRecognizerStateEnded)
    {
        CGFloat panX = [panGes translationInView:self.rightimageView].x;
        CGFloat finalX = currentTranslateX + panX;
        if (finalX > _LeftSJudgeOffset)
        {
//            if (!_canShowLeft||_LeftVC==nil) {
//                return;
//            }
            
            CGAffineTransform conT = [self transformWithDirection];
            [UIView beginAnimations:nil context:nil];
            self.rightimageView.transform = conT;
            [UIView commitAnimations];
            
             _tapGestureRec.enabled = YES;
            return;
        }
        if (finalX < -_RightSJudgeOffset)
        {
//            if (!_canShowRight||_RightVC==nil) {
//                return;
//            }
            
//            CGAffineTransform conT = [self transformWithDirection];
//            [UIView beginAnimations:nil context:nil];
//            _rightView.transform = conT;
//            [UIView commitAnimations];
//            
//            _tapGestureRec.enabled = YES;
            return;
        }
        else
        {
            CGAffineTransform oriT = CGAffineTransformIdentity;
            [UIView beginAnimations:nil context:nil];
            self.rightimageView.transform = oriT;
            [UIView commitAnimations];
            
            _tapGestureRec.enabled = NO;
            _panGestureRec.enabled = NO;
            self.sidemenuViewController.view.hidden = YES;
            self.menuButton.hidden = NO;
            self.containerView.hidden = NO;
            self.rightimageView.hidden = YES;

        }
    } 
}


- (CGAffineTransform)transformWithDirection
{
    float _LeftSContentOffset=160;
    float _LeftSContentScale=0.85;
    
    CGFloat translateX = 0;
    CGFloat transcale = 0;
            translateX = _LeftSContentOffset;
            transcale = _LeftSContentScale;
    
    CGAffineTransform transT = CGAffineTransformMakeTranslation(translateX, 0);
    CGAffineTransform scaleT = CGAffineTransformMakeScale(transcale, transcale);
    CGAffineTransform conT = CGAffineTransformConcat(transT, scaleT);
//    
    return conT;
}


- (NSString*)deviceWithNumString{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    @try {
        return [deviceString stringByReplacingOccurrencesOfString:@"," withString:@""];
    }
    @catch (NSException *exception) {
        return deviceString;
    }
    @finally {
    }
}


- (void)configureViewShadowWithDirection
{
//    if ([[self deviceWithNumString] hasPrefix:@"iPhone"]&&[[[self deviceWithNumString] stringByReplacingOccurrencesOfString:@"iPhone" withString:@""] floatValue]<40) {
//        return;
//    }
//    if ([[self deviceWithNumString] hasPrefix:@"iPod"]&&[[[self deviceWithNumString] stringByReplacingOccurrencesOfString:@"iPod" withString:@""] floatValue]<40) {
//        return;
//    }
//    if ([[self deviceWithNumString] hasPrefix:@"iPad"]&&[[[self deviceWithNumString] stringByReplacingOccurrencesOfString:@"iPad" withString:@""] floatValue]<25) {
//        return;
//    }
    
    CGFloat  shadowW = -2.0f;
    self.rightimageView.layer.shadowOffset = CGSizeMake(shadowW, 1.0);
    self.rightimageView.layer.shadowColor = [UIColor blackColor].CGColor;
        //  _rightView.layer.shadowOpacity = 0.2;
        //_containerView.layer.masksToBounds = YES;
}


-(void)closedMenu
{
    
    
    float _LeftSCloseDuration=0.4;
    
        //  _containerView.layer.shadowOpacity = 1.0f;
    CGAffineTransform oriT = CGAffineTransformIdentity;
    [UIView animateWithDuration:_LeftSCloseDuration
                     animations:^{
                         self.rightimageView.transform = oriT;
                     }
                     completion:^(BOOL finished) {
                         _tapGestureRec.enabled = NO;
                         _panGestureRec.enabled = NO;
                         self.sidemenuViewController.view.hidden = YES;
                         self.menuButton.hidden = NO;
                         self.containerView.hidden = NO;
                         self.rightimageView.hidden = YES;

                     }];
    
}


-(void)menuButtonTapped:(id)sender
{
    [self.sidemenuViewController showViewMenu];
    
//    [_rightView  setImage:[shareDataManager MakeImageFromView:[_containerView subviews].lastObject]];
    [self.rightimageView  setImage:[shareDataManager MakeImageFromView:self.containerView]];
    self.rightimageView.frame = self.containerView.frame;
    self.containerView.hidden = YES;
    self.rightimageView.hidden = NO;
    
    CGAffineTransform conT = [self transformWithDirection];
    
        //    [self.view sendSubviewToBack:_rightSideView];
    [self configureViewShadowWithDirection];
    float _LeftSOpenDuration=0.3;
    
    [UIView animateWithDuration:_LeftSOpenDuration
                     animations:^{
                         self.rightimageView.transform = conT;
                     }
                     completion:^(BOOL finished) {
                               _tapGestureRec.enabled = YES;
                             _panGestureRec.enabled =YES;
                             //            _containerView.layer.shadowOpacity = 0.8f;
                             //_containerView.userInteractionEnabled=NO;
                     }];


    self.sidemenuViewController.view.hidden = !self.sidemenuViewController.view.hidden;
    self.menuButton.hidden = !self.sidemenuViewController.view.hidden;
    
    
}


-(CGRect)viewBounce
{
  _viewBounce  =   CGRectMake(0,
                             TOOLBAR_HEIGHT_TOP,
                             [[UIScreen mainScreen] applicationFrame].size.width,
                             [[UIScreen mainScreen] applicationFrame].size.height-(TOOLBAR_HEIGHT_TOP+TOOLBAR_HEIGHT_BOTTOM-20));
    

   return _viewBounce;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSLog(@"ViewWillAppear: %@", [self.class description]);
    
    if (self.childViewControllers.count == 0) {
                
        UIViewController *root = [_viewControllerStack firstObject];
        
        [self addChildViewController:root];
        
        [root beginAppearanceTransition:YES animated:NO];
        [self.containerView addSubview:root.view];
        [root endAppearanceTransition];
        
        [root didMoveToParentViewController:self];
    }
}

- (void)panGestureEnable:(BOOL)benable
{

    self.interactivePopGestureRecognizer.enabled = benable;

}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSLog(@"ViewDidAppear: %@", [self.class description]);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    NSLog(@"ViewWillDisappear: %@", [self.class description]);
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    NSLog(@"ViewDidDisappear: %@", [self.class description]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutomaticallyForwardAppearanceMethods
{
    /*
     *  This is important.
     *  If you return YES here, the containment-methods (addChildViewController:, etc.) will call appearance methods (viewWillAppear:, etc.)
     *  on the childViewControllers. Problem: They are not really reliable in that case ;). E.g. viewWillAppear:, etc. will not get called properly.
     *  You would most likely get a warning: "Unbalanced calls to appearance methods", when adding / removing childVCs.
     *  Doing it on your own is much more accurate / reliable.
     */
    
    return NO;
}


#pragma mark - UI Interaction

- (void)handlePopGesture:(UIScreenEdgePanGestureRecognizer *)recognizer
{
    if (_viewControllerStack.count < 2 ||  self.sidemenuViewController.view.hidden == NO) {
        //  Nothing to pop here.
        return;
    }
    
    /*
     *  I guess this is self explaining.
     *
     *  Hint: Press CMD+CTRL+J while the cursor (not the mouse ! ;)) is on startInteractiveTransition:, etc. and see where it is going.
     *  In the transitionController class, do the same for [context updateInteractiveTransition:], etc.
     *  Note that you will be jumping back and forth between this class and INCCustomContainerTransition.
     *
     *  This helped me a lot understanding the call hierarchy and relationship.
     *
     *  This only works if your keyboard settings are default.
     *  It also works through 'Navigate > Jump to Definition' in the Menu.
     */
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        
        fromViewController = self.topViewController;
        toViewController = [_viewControllerStack objectAtIndex:_viewControllerStack.count - 2];
        
        [fromViewController willMoveToParentViewController:nil];
        
        [fromViewController beginAppearanceTransition:NO animated:YES];
        [toViewController beginAppearanceTransition:YES animated:YES];
        
        [self.transitionController setPushing:NO];
        [self.transitionController startInteractiveTransition:self];
        
    } else {
        CGPoint location = [recognizer translationInView:self.view];
        CGFloat progress = location.x / self.view.bounds.size.width;
        
        if (recognizer.state == UIGestureRecognizerStateChanged) {
            [self.transitionController updateInteractiveTransition:progress];
            
        } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
            CGPoint velocity = [recognizer velocityInView:self.view];
            
            //  When the left edge of the popped viewController is further left than 0.2 * screenWidth; - cancel popping -
            //  When it is further right than 0.8 * screenWidth; - finish popping -
            //  When it is in between, let the velocity (current pan direction) decide.
            BOOL shouldCancel = progress < 0.2 ? YES : progress > 0.8 ? NO : velocity.x < 0;
            
            [self.transitionController endInteractiveTransition:shouldCancel];
        }
    }

}


#pragma mark - Property Getters & Setters

- (NSArray *)viewControllers
{
    return [NSArray arrayWithArray:_viewControllerStack];
}

- (UIViewController *)topViewController
{
    return (UIViewController *)[_viewControllerStack lastObject];
}

- (INCCustomContainerTransition *)transitionController
{
    /*
     *  Create lazily.
     */
    if (! _transitionController) {
        _transitionController = [[INCCustomContainerTransition alloc] init];
    }
    
    return _transitionController;
}


-(NSMutableArray*)webviewControllerStack
{
    if (_webviewControllerStack == nil) {
        _webviewControllerStack = [NSMutableArray arrayWithCapacity:100];
    }
    
    
    return _webviewControllerStack;
}


#pragma mark - Manipulating Stack

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    /*
     *  Store Ivars to return them later in the transitionContext delegate
     */
    toViewController = viewController;
    fromViewController = self.topViewController;
 
    
    [_viewControllerStack addObject:viewController];
    
    if ([[viewController class] isSubclassOfClass:[KHUIWebViewController class]]) {
        [self.webviewControllerStack addObject:viewController]; 
    }
    
    /*
     *  The common squence of calls to swap to childViewControllers.
     *  If animated == NO, simply finish the transition.
     *  If animated == YES, start the transition, let the transitionController do the animation
     *  and finalize the transition below in 'completeTransition:' of the TransitioningContext Delegate.
     */
    
    [self addChildViewController:toViewController];
    
    [fromViewController beginAppearanceTransition:NO animated:animated];
    [toViewController beginAppearanceTransition:YES animated:animated];
    
    if (! animated) {
        /*
         *  Note that this is more or less the same as below in completeTransition:
         *  The animation is just the inner part of the transition, encapsulated by the appearance and containment calls.
         */
        
        [self.containerView addSubview:toViewController.view];
        [fromViewController.view removeFromSuperview];
        
        [fromViewController endAppearanceTransition];
        [toViewController endAppearanceTransition];
        
        [toViewController didMoveToParentViewController:self];
        
        /*
         *  Important thing here: Remove the disappearing ViewController's view (here: fromViewController) from the _containerView
         *  but do NOT remove the controller as a child. It stays a child, it's view is just not visible anymore.
         *  Only call removeFromParentViewController: on viewControllers when they are disappearing
         *  and not coming back. (When they are removed from the stack)
         *  Example:
         *  popping a viewController CAN remove the fromVC, because it is not going to come back.
         *  pushing MUST NOT remove fromVC, because it stays in the _viewControllerStack in that case.
         */
//        [fromViewController removeFromParentViewController]; <- WRONG !!
        
            //      [self.titleLabel setText:toViewController.title];
        
        //  Remember to set them back to nil so they can be released by ARC.
        fromViewController  = nil;
        toViewController    = nil;
    } else {
        [self.transitionController setPushing:YES];
        
        [self.transitionController animateTransition:self];
    }
}


#pragma mark - UIViewControllerContextTransitioning

/*
 *  As this is supposed to be a short example, nothing more:
 *  Most of the methods don't return useful values, mostly because the INCCustomControllerTransition does not need them.
 *  
 *  When implementing a customContainer, you should return real values here.
 */


- (BOOL)isAnimated
{
    // Return fake value
    return NO;
}

- (BOOL)isInteractive
{
    // Return fake value
    return NO;
}

- (BOOL)transitionWasCancelled
{
    // Return fake value
    return NO;
}

- (UIModalPresentationStyle)presentationStyle
{
    // Return fake value
    return UIModalPresentationCustom;
}

- (void)updateInteractiveTransition:(CGFloat)percentComplete
{
    /*
     *  This is where containers update the transition for their supplementary views.
     *
     *  For example, a UINavigationController will move around the titles and barButtons in it's NavigationBar.
     *
     *  This container could crossfade between the old titleLabel value and the new one.
     *
     *  It's totally up to you what your supplementary views do during the transition.
     */
}

- (void)finishInteractiveTransition
{
    /*
     *  This is where containers finsih the transition for their supplementary views.
     *
     *  For example, a UINavigationController will finish the animation in it's NavigationBar.
     */
}

- (void)cancelInteractiveTransition
{
    /*
     *  This is where containers cancel the transition for their supplementary views.
     *
     *  For example, a UINavigationController will cancel the animation in it's NavigationBar.
     */
    
    //  Reverse the appearanceTranstion.
    [fromViewController beginAppearanceTransition:YES animated:YES];
    [toViewController beginAppearanceTransition:NO animated:YES];
}

- (void)completeTransition:(BOOL)didComplete
{
    /*
     *  This is the method is called by the id<UIViewControllerAnimated/InteractiveTransitioning> object when the animation / interaction
     *  completes.
     */
    
    if (didComplete) {
        //  The 'toViewController.view' is added by in the animateTransition: method of the transitionController.
        [fromViewController.view removeFromSuperview];
        
        //  Finalize the appearanceTransition
        [fromViewController endAppearanceTransition];
        [toViewController endAppearanceTransition];
        
        
        //  Update supplementary views
        [self.titleLabel setText:toViewController.title];
        
        
        if (self.transitionController.pushing) {
            
            //  Finalize the viewController transition
            [toViewController didMoveToParentViewController:self];
            
            //  Update the stack
            
            //  Not needed here, because the pushViewController:animated: method already added the toViewController to the stack.
            //  Typically, it's best to upate the stack as SOON as possible when doing animated transitions (not cancelable)
            //  and as LATE as possible for interactive transitions (cancelable)
            //  This makes sure the stack is in a valid state as long as possible.
            //        [_viewControllerStack addObject:toViewController];
        } else {
            
            //  Finalize the viewController transition
            [fromViewController removeFromParentViewController];
            
            //  Update the stack
            [_viewControllerStack removeLastObject];
        }
    } else {
        //  Finalize the appearanceTransition
        [fromViewController endAppearanceTransition];
        [toViewController endAppearanceTransition];
    }
    
    //  Set Ivars to nil to release their memory (if this reference was the last one)
    //  If you do not set them to nil, popped viewControllers will stay alive (not deallocated, event with ARC)
    //  until the next transition happens, and the Ivar are set to new values.
    fromViewController  = nil;
    toViewController    = nil;
}


- (void)completeTransition
{
    fromViewController = self.topViewController;
    toViewController = [_viewControllerStack objectAtIndex:_viewControllerStack.count - 2];
    
    [fromViewController willMoveToParentViewController:nil];
//    
    [fromViewController beginAppearanceTransition:NO animated:YES];
    [toViewController beginAppearanceTransition:YES animated:YES];
    
    [self.transitionController setPushing:NO];
    [self.transitionController startInteractiveTransition:self];


    [self.transitionController updateInteractiveTransition:1.0];
    
      [self.transitionController endInteractiveTransition:NO];

   // [self completeTransition:YES];

}


- (UIViewController *)viewControllerForKey:(NSString *)key
{
    if ([key isEqualToString:UITransitionContextFromViewControllerKey]) {
        return fromViewController;
    } else {
        return toViewController;
    }
}

- (CGRect)initialFrameForViewController:(UIViewController *)vc
{
    // Return fake value
    return CGRectZero;
}

- (CGRect)finalFrameForViewController:(UIViewController *)vc
{
    // Return fake value
    return CGRectZero;
}

@end



@implementation UIViewController (INCCustomContainer)

- (INCCustomContainer *)customContainer
{
    if ([self isKindOfClass:[INCCustomContainer class]]) {
        return (INCCustomContainer *)self;
    }
    
    UIViewController *parent = self.parentViewController;
    
    while (! [parent isKindOfClass:[INCCustomContainer class]] && parent != nil) {
        parent = parent.parentViewController;
    }
    
    return (INCCustomContainer *)parent;
}

@end